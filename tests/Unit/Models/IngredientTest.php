<?php

namespace Tests\Unit\Models;

use App\Models\Ingredient;
use Carbon\Carbon;
use Tests\TestCase;

class IngredientTest extends TestCase
{
    public function testSetCheckedByAttributeSetsAllPropertiesAndRemovesCheckReason()
    {
        /** @var Ingredient $ingredient */
        $ingredient = factory(Ingredient::class)->create();
        $userId = 1;

        $this->assertTrue($ingredient->checked_by === null);
        $this->assertTrue($ingredient->checked_at === null);
        $this->assertFalse($ingredient->is_checked);

        $ingredient->check_reason = Ingredient::INGREDIENT_UPDATED;
        $ingredient->setCheckedByAttribute($userId);
        
        $this->assertTrue($ingredient->checked_by === $userId);
        $this->assertTrue($ingredient->is_checked);
        $this->assertNull($ingredient->check_reason);
        $this->assertInstanceOf(Carbon::class, $ingredient->checked_at);
    }

    public function testRemoveCheckedAttributeRemovesAllPropertiesAndSetsReason()
    {
        /** @var Ingredient $ingredient */
        $ingredient = factory(Ingredient::class)->create();
        $userId = 1;
        $checkReason = $ingredient::INGREDIENT_UPDATED;

        $ingredient->setCheckedByAttribute($userId);
        $ingredient->removeCheckedAttribute($checkReason);

        $this->assertNull($ingredient->checked_by);
        $this->assertNull($ingredient->checked_at);
        $this->assertTrue($ingredient->check_reason === $checkReason);
        $this->assertFalse($ingredient->is_checked);
    }

    public function testGetValidatorForUserMatchesConfigUnits()
    {
        /** @var Ingredient $ingredient */
        $ingredient = factory(Ingredient::class)->create();

        $units = config('ingredients.units');

        foreach ($units as $unit) {
            $this->assertTrue($ingredient->getValidatorForUser([
                'name'  => 'Любое Название',
                'units' => $unit,
            ])->passes());
        }
    }

    public function testGetValidatorForUserFailsInvalidData()
    {
        /** @var Ingredient $ingredient */
        $ingredient = factory(Ingredient::class)->create();

        $this->assertTrue($ingredient->getValidatorForUser([
            'name'  => 'Любое название',
            'units' => 'ошибочное',
        ])->fails(), 'Неверные единицы измерения не проходят валидацию.');

        $this->assertTrue($ingredient->getValidatorForUser([
            'units' => 'шт',
        ])->fails(), 'Ингредиент без поля name не проходит валидацию.');
    }

    /**
     * @dataProvider moderatorValidationData
     */
    public function testGetValidatorForModerator($data, bool $expectedResult, $message)
    {
        /** @var Ingredient $ingredient */
        $ingredient = factory(Ingredient::class)->create();

        $validation = $ingredient->getValidatorForModerator($data)->passes();

        if ($expectedResult) {
            $this->assertTrue($validation, $message);
        } else {
            $this->assertFalse($validation, $message);
        }

    }

    public function moderatorValidationData()
    {
        return [
            [
                [
                    'name'                  => 'Какая-то крупа',
                    'units'                 => 'гр',
                    'is_loose'              => true,
                    'amount_in_glass_200ml' => 220,
                    'min_amount_to_buy'     => 1,
                    'comment'               => '',
                    'calories'              => 300.00,
                    'proteins'              => 100.00,
                    'fats'                  => 100.00,
                    'carbohydrates'         => 100.00,
                    'vegan_component'       => true,
                    'vegetarian_component'  => true,
                    'avg_unit_cost'         => 50,
                ],
                true,
                'Все верно',
            ],
            [
                [
                    'name'                  => 'Какая-то крупа',
                    'units'                 => 'гр',
                    'is_loose'              => false,
                    'amount_in_glass_200ml' => null,
                    'min_amount_to_buy'     => 1,
                    'comment'               => '',
                    'calories'              => 300.00,
                    'proteins'              => 100.00,
                    'fats'                  => 100.00,
                    'carbohydrates'         => 100.00,
                    'vegan_component'       => true,
                    'vegetarian_component'  => true,
                    'avg_unit_cost'         => 50,
                ],
                true,
                'Все верно (Параметр amount_in_glass_200ml занулен)',
            ],
            [
                [
                    'name'                 => 'Какая-то крупа',
                    'units'                => 'гр',
                    'is_loose'             => true,
                    'min_amount_to_buy'    => 1,
                    'comment'              => '',
                    'calories'             => 300.00,
                    'proteins'             => 100.00,
                    'fats'                 => 100.00,
                    'carbohydrates'        => 100.00,
                    'vegan_component'      => true,
                    'vegetarian_component' => true,
                    'avg_unit_cost'        => 50,
                ],
                false,
                'Не указана масса стакана',
            ],
            [
                [
                    'name'                 => 'Какая-то крупа',
                    'units'                => 'шт',
                    'is_loose'             => false,
                    'min_amount_to_buy'    => 1,
                    'comment'              => '',
                    'calories'             => 300.00,
                    'proteins'             => 100.00,
                    'fats'                 => 100.00,
                    'carbohydrates'        => 100.00,
                    'vegan_component'      => true,
                    'vegetarian_component' => true,
                    'avg_unit_cost'        => 50,
                ],
                false,
                'Не указан вес 1 шт',
            ],
            [
                [
                    'name'                 => 'Какая-то крупа',
                    'units'                => 'шт',
                    'is_loose'             => false,
                    'min_amount_to_buy'    => 1,
                    'comment'              => '',
                    'calories'             => 300.00,
                    'proteins'             => 100.00,
                    'fats'                 => 100.00,
                    'carbohydrates'        => 100.00,
                    'vegan_component'      => true,
                    'vegetarian_component' => true,
                    'avg_unit_cost'        => 50,
                    'avg_piece_weight'     => 100,
                ],
                true,
                'Указан вес 1 шт',
            ],
            [
                [
                    'name'                 => 'Какая-то крупа',
                    'units'                => 'гр',
                    'is_loose'             => false,
                    'min_amount_to_buy'    => 1,
                    'comment'              => '',
                    'calories'             => 300.00,
                    'proteins'             => 100.00,
                    'fats'                 => 100.00,
                    'carbohydrates'        => 100.00,
                    'vegan_component'      => true,
                    'vegetarian_component' => true,
                    'avg_unit_cost'        => 50,
                    'avg_piece_weight'     => null, // null ставится мидлваром, если параметр пуст
                ],
                true,
                'Не указан вес 1 шт, но он не требуется (проверка необязательности параметра)',
            ],
            [
                [
                    'name'                 => 'Какая-то крупа',
                    'units'                => 'крупинка',
                    'is_loose'             => false,
                    'min_amount_to_buy'    => 1,
                    'comment'              => '',
                    'calories'             => 300.00,
                    'proteins'             => 100.00,
                    'fats'                 => 100.00,
                    'carbohydrates'        => 100.00,
                    'vegan_component'      => true,
                    'vegetarian_component' => true,
                    'avg_unit_cost'        => 50,
                ],
                false,
                'Не указаны верные единицы измерения',
            ],
        ];
    }

    /**
     * @dataProvider modelFillingData
     */
    public function testCheckModelFilling($data, $role, $expectedResult, $message)
    {
        /** @var Ingredient $ingredient */
        $ingredient = factory(Ingredient::class)->create($data);

        $validation = $ingredient->checkModelFilling($role);

        $this->assertTrue($validation->isFilled === $expectedResult->isFilled, $message);
        $this->assertEquals($expectedResult->emptyParam, $validation->emptyParam, $message);
    }

    public function modelFillingData()
    {
        return [
            [
                [
                    'name'  => 'Творог',
                    'units' => 'шт',
                ],
                'user',
                (object)[
                    'isFilled'   => true,
                    'emptyParam' => null,
                ],
                'Все заполнено верно',
            ],
            [
                [
                    'name'  => 'Творог',
                    'units' => 'юниты',
                ],
                'user',
                (object)[
                    'isFilled'   => false,
                    'emptyParam' => 'units',
                ],
                'Неверное заполнение параметра units',
            ],
            [
                [
                    'name'                 => 'Какая-то крупа',
                    'units'                => 'гр',
                    'is_loose'             => true,
                    'min_amount_to_buy'    => 1,
                    'comment'              => '',
                    'calories'             => 300.00,
                    'proteins'             => 100.00,
                    'fats'                 => 100.00,
                    'carbohydrates'        => 100.00,
                    'vegan_component'      => true,
                    'vegetarian_component' => true,
                    'avg_unit_cost'        => 50,
                ],
                'moderator',
                (object)[
                    'isFilled'   => false,
                    'emptyParam' => 'amount_in_glass_200ml',
                ],
                'Не указана масса стакана',
            ],
        ];
    }
}
