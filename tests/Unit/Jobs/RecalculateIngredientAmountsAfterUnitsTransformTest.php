<?php

namespace Tests\Unit\Jobs;

use App\Exceptions\IngredientUnitConversionException;
use App\Jobs\Ingredient\RecalculateIngredientAmountsAfterUnitsTransform;
use App\Models\Ingredient;
use App\Models\Recipe;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RecalculateIngredientAmountsAfterUnitsTransformTest extends TestCase
{
    /**
     * @dataProvider conversionValidatorData
     */
    public function testConversionAvailableValidatorAssertsTrue($from, $to) {
        $this->assertTrue(RecalculateIngredientAmountsAfterUnitsTransform::conversionAvailable($from, $to), "Можно перегонять из $from в $to");
    }

    public function conversionValidatorData() {
        return [
            [
                'from' => 'шт',
                'to' => 'гр',
            ],
            [
                'from' => 'шт',
                'to' => 'кг',
            ],
            [
                'from' => 'гр',
                'to' => 'кг',
            ],
            [
                'from' => 'гр',
                'to' => 'шт',
            ],
            [
                'from' => 'кг',
                'to' => 'гр',
            ],
            [
                'from' => 'кг',
                'to' => 'шт',
            ],
        ];
    }

    /**
     * @dataProvider conversionInvalidValidatorData
     */
    public function testConversionAvailableValidatorAssertsException($from, $to) {
        $this->expectException(IngredientUnitConversionException::class);

        dispatch_now(
            new RecalculateIngredientAmountsAfterUnitsTransform(1, $from,$to)
        );
    }

    public function conversionInvalidValidatorData() {
        return [
            [
                'from' => 'шт',
                'to' => 'шт',
            ],
            [
                'from' => 'гр',
                'to' => 'мл',
            ],
        ];
    }

    public function testConvertAmountUnitsAssertsTrue()
    {
        $ingredient1 = factory(Ingredient::class)->create([
            'name'  => 'Спагетти',
            'units' => 'гр',
        ]);
        $ingredient2 = factory(Ingredient::class)->create([
            'name'  => 'Кетчуп',
            'units' => 'шт', // сферический одноразовый пакетик кетчупа на 25 грамм
        ]);
        $ingredient3 = factory(Ingredient::class)->create([
            'name'  => 'Спагетти 2',
            'units' => 'гр',
        ]);
        $ingredient4 = factory(Ingredient::class)->create([
            'name'  => 'Кетчуп 2',
            'units' => 'шт',
        ]);

        $recipe = factory(Recipe::class)->create([
            'name' => 'Спагетти с кетчупом',
            'slug' => 'spag1',
        ]);
        $recipe2 = factory(Recipe::class)->create([
            'name' => 'Спагетти с кетчупом 2',
            'slug' => 'spag2',
        ]);

        $recipe->ingredients()->attach([
            $ingredient1->id => ['amount' => 100],
            $ingredient2->id => ['amount' => 2],
        ]);
        $recipe2->ingredients()->attach([
            $ingredient3->id => ['amount' => 100],
            $ingredient4->id => ['amount' => 2],
        ]);

        $this->assertTrue((int)$recipe->ingredients[0]->pivot->amount === 100);
        $this->assertTrue((int)$recipe->ingredients[1]->pivot->amount === 2);

        $this->assertTrue((int)$recipe2->ingredients[0]->pivot->amount === 100);
        $this->assertTrue((int)$recipe2->ingredients[1]->pivot->amount === 2);

        // переводим спагетти в пачки по полкило, а кетчуп в граммы
        dispatch_now(new RecalculateIngredientAmountsAfterUnitsTransform($ingredient1->id, 'гр',
            'шт', 500));
        dispatch_now(new RecalculateIngredientAmountsAfterUnitsTransform($ingredient2->id, 'шт',
            'гр', 25));

        // аналогично тестируем пеервод с килограммами
        dispatch_now(new RecalculateIngredientAmountsAfterUnitsTransform($ingredient3->id, 'гр',
            'кг'));
        dispatch_now(new RecalculateIngredientAmountsAfterUnitsTransform($ingredient4->id, 'шт',
            'кг', 25));


        $recipe->load('ingredients');
        $recipe2->load('ingredients');

        $this->assertTrue(
            (double)$recipe->ingredients[0]->pivot->amount === 0.2,
            'Превращены 100 гр в шт по 500 гр'
        );
        $this->assertTrue(
            (int)$recipe->ingredients[1]->pivot->amount === 50,
            'Превращены 2 шт по 25 гр в гр'
        );

        $this->assertTrue(
            (double)$recipe2->ingredients[0]->pivot->amount === 0.1,
            'Превращены 100 гр в кг'
        );
        $this->assertTrue(
            (double)$recipe2->ingredients[1]->pivot->amount === 0.05,
            'Превращены 2 шт по 25 гр в кг'
        );


    }
}
