<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'api'], function () {

    Route::apiResource('recipe-categories', 'API\RecipeCategoryController');
    Route::get('ingredient', 'API\IngredientController@index');
    Route::get('cooking-types', 'API\CookingTypeController@index');

    Route::get('recipe/total-cost-under/{amount}', 'API\RecipeController@withTotalCostUnder')
        ->where('amount', '[0-9]+');
    Route::get('recipe/portion-cost-under/{amount}', 'API\RecipeController@withPortionCostUnder')
        ->where('amount', '[0-9]+');

    Route::get('recipe/search', 'API\RecipeController@search');
    Route::get('recipe/{id}', 'API\RecipeController@getRecipeById')
        ->where('id', '[0-9]+');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('logout', 'Auth\LoginController@logout');
        Route::post('refresh', 'Auth\LoginController@refresh');

        Route::get('/user', function (Request $request) {
            return $request->user();
        });

        Route::patch('settings/profile', 'Settings\ProfileController@update');
        Route::patch('settings/password', 'Settings\PasswordController@update');

        Route::get('recipe/user-preferences', 'API\RecipeController@getRecipePreferencesData');

        Route::post('recipe/favorites/add', 'API\RecipeController@addToFavorite');
        Route::post('recipe/favorites/remove', 'API\RecipeController@removeFromFavorite');
        Route::get('recipe/favorites', 'API\RecipeController@getFavorites');

        Route::post('recipe/postponed/add', 'API\RecipeController@addToPostponed');
        Route::post('recipe/postponed/remove', 'API\RecipeController@removeFromPostponed');
        Route::get('recipe/postponed', 'API\RecipeController@getPostponed');

        Route::group(['prefix' => 'admin'], function () {
            Route::get('recipe/list', 'API\RecipeController@list');
            Route::delete('recipe/destroy/{id}', 'API\RecipeController@destroy');
        });

        Route::group(['prefix' => 'dashboard'], function () {
            Route::get('ingredient/unapproved',
                'API\IngredientController@unapproved')->middleware('can:can_approve_ingredients');
            Route::post('ingredient/decline/{ingredient}', 'API\IngredientController@decline')
                ->middleware('can:can_approve_ingredients');
            Route::apiResource('ingredient', 'API\IngredientController')->only([
                'store',
                'update',
                // 'destroy',
            ]);

            Route::get('recipe/my', 'API\RecipeController@getUserOwnRecipes');
            Route::get('recipe/unapproved',
                'API\RecipeController@unapproved')->middleware('can:can_approve_recipes');
            Route::get('recipe/similar', 'API\RecipeController@findSimilarRecipes');
            Route::post('recipe/decline/{id}',
                'API\RecipeController@decline')->middleware('can:can_approve_recipes');
            Route::apiResource('recipe', 'API\RecipeController')->only([
                'store',
                'update',
                'destroy'
            ]);
        });


    });

    Route::group(['middleware' => 'guest:api'], function () {
        Route::post('login', 'Auth\LoginController@login');
        Route::post('register', 'Auth\RegisterController@register');

        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset');

        Route::post('oauth/{provider}', 'Auth\OAuthController@redirectToProvider');
        Route::get('oauth/{provider}/callback',
            'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
    });
});