<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver'  => 'gd',

    // Настройки для сохранения изображений
    'recipes' => [

        'type' => 'jpg',

        'size' => [
            'width'  => 1600,
            'height' => 900,
        ],

        'thumb' => [
            'width'  => 330,
            'height' => 200,
        ],
    ],

];
