<?php
/**
 * User: Tantacula
 * Date: 03.07.2019
 * Time: 3:12
 */

return [
    'units' => [
        'гр',
        'кг',
        'шт',
        'л',
        'мл',
    ],
];