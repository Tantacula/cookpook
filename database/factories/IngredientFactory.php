<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Ingredient::class, function (Faker $faker) {
    return [
        'name'     => 'Спагетти',
        'units'    => 'гр',
        'checked_at'=> null,
    ];
});
