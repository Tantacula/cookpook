<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Recipe::class, function (Faker $faker) {
    return [
//        'author_id' => null,
        'name' => 'Салат',
//        'category_id' => null,
        'slug' => 'salad',
        'article_text'=> 'Накрошить салад',
        'image' => '',
        'cooking_time_minutes' => 10,
    ];
});
