<?php

use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        \DB::table((new \App\Models\Ingredient())->getTable())->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        \App\Models\Ingredient::unguard();

        $ingredients = [
            [
                'name'                 => 'Спагетти',
                'units'                => 'гр',
                'min_amount_to_buy'    => 500,
                'calories'             => 359,
                'proteins'             => 14,
                'carbohydrates'        => 70,
                'fats'                 => 2,
                'vegan_component'      => false,
                'vegetarian_component' => true,
                'avg_unit_cost'        => 60,
                'checked_at'           => \Carbon\Carbon::now(),
            ],
        ];
        foreach ($ingredients as $item) {
            \App\Models\Ingredient::create($item);
        }

        \App\Models\Ingredient::reguard();
    }
}
