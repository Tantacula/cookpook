<?php

use Illuminate\Database\Seeder;

class CookingTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        \DB::table((new \App\Models\CookingType())->getTable())->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        \App\Models\CookingType::unguard();

        $types = [
            [
                'name' => 'Жарить (тушить) на сковороде',
                'slug' => 'fried',
            ],
            [
                'name' => 'Варить (тушить) в кастрюле',
                'slug' => 'boiled',
            ],
            [
                'name' => 'Запечь в духовке',
                'slug' => 'baked',
            ],
            [
                'name' => 'Просто смешать ингредиенты',
                'slug' => 'cold',
            ],
            [
                'name' => 'Приготовить в микроволновке',
                'slug' => 'microwave',
            ],
            [
                'name' => 'Измельчить в блендере',
                'slug' => 'blender',
            ],
            [
                'name' => 'Измельчить в мясорубке',
                'slug' => 'meatgrinder',
            ],
            [
                'name' => 'Другое',
                'slug' => 'other',
            ],
            [
                'name' => 'Картофелемялка',
                'slug' => 'masher',
            ],
            [
                'name' => 'Циновка для роллов',
                'slug' => 'mat-for-sushi',
            ],
        ];
        foreach ($types as $item) {
            \App\Models\CookingType::create($item);
        }

        \App\Models\CookingType::reguard();
    }
}
