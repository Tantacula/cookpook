<?php

use Illuminate\Database\Seeder;

class RecipeCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        \DB::table((new \App\Models\RecipeCategory())->getTable())->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        \App\Models\RecipeCategory::unguard();

        $categories = [
            'Первое блюдо',
            'Второе блюдо',
            'Десерт',
            'Выпечка',
            'Сладкая выпечка',
            'Салат',
            'Соус',
            'Холодные напитки',
            'Горячие напитки',
            'Бутерброды',
            'Другое',
        ];
        foreach ($categories as $cat) {
            \App\Models\RecipeCategory::create(['name' => $cat,]);
        }

        \App\Models\RecipeCategory::reguard();
    }
}
