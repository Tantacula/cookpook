<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheckReasonFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recipes', function (Blueprint $table) {
            $table->string('check_reason')->nullable();
        });

        Schema::table('ingredients', function (Blueprint $table) {
            $table->string('check_reason')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recipes', function (Blueprint $table) {
            $table->dropColumn('check_reason');
        });

        Schema::table('ingredients', function (Blueprint $table) {
            $table->dropColumn('check_reason');
        });
    }
}
