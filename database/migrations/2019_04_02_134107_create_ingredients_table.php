<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('units');
            // сыпучий продукт
            $table->boolean('is_loose')->default(0);
            // если сыпучий, то сколько граммов в граненом стакане
            $table->unsignedInteger('amount_in_glass_200ml')->nullable();
            // минимальное количество, которое можно купить
            $table->unsignedDecimal('min_amount_to_buy')->nullable();
            $table->string('comment')->nullable();
            // калории и бжу на 100 гр
            $table->unsignedDecimal('calories', 10, 2)->nullable();
            $table->unsignedDecimal('proteins', 5, 2)->nullable();
            $table->unsignedDecimal('fats', 5, 2)->nullable();
            $table->unsignedDecimal('carbohydrates', 5, 2)->nullable();
            $table->boolean('vegan_component')->nullable();
            $table->boolean('vegetarian_component')->nullable();
            $table->unsignedDecimal('avg_unit_cost')->nullable();
            $table->dateTime('cost_added_at')->nullable();
            $table->unsignedInteger('avg_piece_weight')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('checked_by')->nullable();
            $table->dateTime('checked_at')->nullable();
            $table->timestamps();

            $table->foreign('checked_by')
                ->references('id')
                ->on('users')
                ->onDelete('set null');

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredients');
    }
}
