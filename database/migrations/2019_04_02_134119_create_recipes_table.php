<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('author_id')->nullable();
            $table->string('name');
            $table->unsignedBigInteger('category_id')->nullable();
            $table->string('slug')->unique();
            $table->string('article_text');
            $table->string('image');
            $table->boolean('is_published')->default(false);
            $table->unsignedInteger('portions_amount')->default(1);
            // общий вес блюда либо его литраж, в мл или гр.
            $table->unsignedDecimal('total_amount', 10, 2)->nullable();
            $table->unsignedDecimal('total_cost')->nullable();
            $table->unsignedDecimal('portion_cost', 8, 2)->nullable();
            $table->dateTime('cost_calculated_at')->nullable();
            $table->dateTime('ingredients_cost_updated_at')->nullable();
            $table->unsignedInteger('cooking_time_minutes');
            // калории считаются автоматически. Калории на все порции, не на одну, т.к. в будущем объем порции может быть изменен
            $table->unsignedDecimal('calories', 10, 2)->nullable();
            $table->unsignedDecimal('proteins', 5, 2)->nullable();
            $table->unsignedDecimal('fats', 5, 2)->nullable();
            $table->unsignedDecimal('carbohydrates', 5, 2)->nullable();
            // свойства вегов пересчитывается при создании рецепта или при изменении компонентов
            $table->boolean('is_vegan')->default(false);
            $table->boolean('is_vegetarian')->default(false);
            // количество оценок. Денормализуется для уменьшения нагрузки, как и предыдущие свойства
            $table->unsignedInteger('marks_amount')->default(0);
            $table->unsignedBigInteger('checked_by')->nullable();
            $table->dateTime('checked_at')->nullable();
            $table->timestamps();

            $table->foreign('author_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');

            $table->foreign('category_id')
                ->references('id')
                ->on('recipe_categories')
                ->onDelete('set null');

            $table->foreign('checked_by')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}
