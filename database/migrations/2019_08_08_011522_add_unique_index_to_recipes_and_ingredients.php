<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueIndexToRecipesAndIngredients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::beginTransaction();
        Schema::table('recipes', function (Blueprint $table) {
            $table->unique('name');
        });
        Schema::table('ingredients', function (Blueprint $table) {
            $table->unique('name');
        });
        \DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::beginTransaction();
        Schema::table('recipes', function (Blueprint $table) {
            $table->dropUnique('recipes_name_unique');
        });
        Schema::table('ingredients', function (Blueprint $table) {
            $table->dropUnique('ingredients_name_unique');
        });
        \DB::commit();
    }
}
