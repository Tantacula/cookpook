<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCookingTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cooking_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            // baked, fried, boiled, cold (имеется в виду, требуется лишь порезать ингредиенты)
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('cooking_type_recipe', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('recipe_id');
            $table->unsignedBigInteger('cooking_type_id');
            $table->timestamps();

            $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
            $table->foreign('cooking_type_id')->references('id')->on('cooking_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cooking_type_recipe');
        Schema::dropIfExists('cooking_types');
    }
}
