<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFulltextIndexToRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('app.env') !== 'testing') {
            DB::statement('ALTER TABLE recipes ADD FULLTEXT name_fulltext(name)');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(config('app.env') !== 'testing') {
            Schema::table('recipes', function (Blueprint $table) {
                $table->dropIndex('name_fulltext');
            });
        }
    }
}
