<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeclineFiledsToRecipesAndIngredients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recipes', function (Blueprint $table) {
            $table->boolean('is_declined')->nullable()->after('checked_at');
            $table->string('decline_reason')->nullable()->after('checked_at');
            $table->unsignedBigInteger('declined_by')->nullable();

            $table->foreign('declined_by')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
            $table->index('is_declined');
        });

        Schema::table('ingredients', function (Blueprint $table) {
            $table->boolean('is_declined')->nullable()->after('checked_at');
            $table->string('decline_reason')->nullable()->after('checked_at');
            $table->unsignedBigInteger('declined_by')->nullable()->after('checked_at');

            $table->foreign('declined_by')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
            $table->index('is_declined');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ingredients', function (Blueprint $table) {
            $table->dropForeign(['declined_by']);
            $table->dropIndex('ingredients_is_declined_index');
            $table->dropColumn(['is_declined', 'decline_reason', 'declined_by']);
        });
        Schema::table('recipes', function (Blueprint $table) {
            $table->dropForeign(['declined_by']);
            $table->dropIndex('recipes_is_declined_index');
            $table->dropColumn(['is_declined', 'decline_reason', 'declined_by']);
        });
    }
}
