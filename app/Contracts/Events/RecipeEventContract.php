<?php namespace App\Contracts\Events;

/**
 * User: Tantacula
 * Date: 14.06.2019
 * Time: 16:41
 */

interface RecipeEventContract
{
    public function getRecipeId(): int;
}