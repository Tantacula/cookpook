<?php

namespace App\Models;

use App\Models\Traits\UserRecipeRelations;
use App\OAuthProvider;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, UserRecipeRelations;

    protected $fillable = [
        'name',
        'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'photo_url',
        'can_approve_ingredients',
        'can_approve_recipes',
    ];

    /**
     * Get the profile photo URL attribute.
     *
     * @return string
     */
    public function getPhotoUrlAttribute()
    {
        return 'https://www.gravatar.com/avatar/' . md5(strtolower($this->email)) . '.jpg?s=200&d=mm';
    }

    /**
     * Get the oauth providers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function oauthProviders()
    {
        return $this->hasMany(OAuthProvider::class);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * @return int
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function hasRole($role)
    {
        switch ($role) {
            // роль user имеет любой пользователь
            case 'user':
                return true;
            // роль moderator имеет модератор или админ
            case 'moderator':
                return $this->role === 'moderator' || $this->role === 'admin';
            // роль админа имеет лишь админ
            case 'admin':
                return $this->role === 'admin';
            default:
                return false;
        }
    }

    public function getCanApproveRecipesAttribute()
    {
        return $this->hasRole('moderator');
    }

    public function getCanApproveIngredientsAttribute()
    {
        return $this->hasRole('moderator');
    }
}
