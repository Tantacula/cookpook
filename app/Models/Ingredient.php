<?php

namespace App\Models;

use App\Exceptions\Eloquent\InvalidAttributeException;
use App\Models\Traits\IngredientValidator;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    use IngredientValidator;

    public const INGREDIENT_CREATED = 'ingredient-created';
    public const INGREDIENT_UPDATED = 'ingredient-updated';

    protected $appends = [
        'is_checked',
    ];

    protected $casts = [
        'is_loose'             => 'boolean',
        'vegan_component'      => 'boolean',
        'vegetarian_component' => 'boolean',
        'is_declined'          => 'boolean',
        'calories'             => 'decimal:2',
        'proteins'             => 'decimal:2',
        'fats'                 => 'decimal:2',
        'carbohydrates'        => 'decimal:2',
        'min_amount_to_buy'    => 'decimal:2',
        'avg_unit_cost'        => 'decimal:2',
    ];

    protected $dates = [
        'checked_at',
        'cost_added_at',
    ];

    public function getIsCheckedAttribute()
    {
        return !is_null($this->checked_at) || !is_null($this->checked_by);
    }

    public function setCheckedByAttribute($userId)
    {
        $this->attributes['checked_by'] = $userId;
        $this->checked_at = Carbon::now();
        $this->check_reason = null;
    }

    public function removeCheckedAttribute($reason = null) {
        $this->checked_by = null;
        $this->checked_at = null;
        $this->check_reason = $reason;
    }

    public function decline($reason, $userId) {
        $this->is_declined = true;
        $this->decline_reason = $reason;
        $this->declined_by = $userId;
    }

    public function recipes()
    {
        return $this->belongsToMany(Recipe::class);
    }

    public function setUnitsAttribute($val)
    {
        $units = mb_strtolower($val);
        // см конфиг ingredients.units
        // todo добавить в базу поле из определенных значений для единиц. Универсальные для языков
        $grammArr = ['гр', 'гр.', 'грам', 'грамм', 'граммы', 'граммов'];
        if (in_array($units, $grammArr)) {
            $this->attributes['units'] = 'гр';
            return;
        }
        $pieceArr = ['шт', 'штук', 'штука', 'шт.'];
        if (in_array($units, $pieceArr)) {
            $this->attributes['units'] = 'шт';
            return;
        }
        $pieceArr = ['л', 'литр', 'литров'];
        if (in_array($units, $pieceArr)) {
            $this->attributes['units'] = 'л';
            return;
        }
        $pieceArr = ['мл', 'миллилитр', 'миллилитров'];
        if (in_array($units, $pieceArr)) {
            $this->attributes['units'] = 'мл';
            return;
        }
        $this->attributes['units'] = $units;
    }

    /**
     * @param string/null $reason
     * @throws InvalidAttributeException
     */
    public function setCheckReason($reason)
    {
        $allowedReasons = [
            null,
            self::INGREDIENT_CREATED,
            self::INGREDIENT_UPDATED,
        ];
        if (!in_array($reason, $allowedReasons)) {
            throw new InvalidAttributeException('Неверная причина для проверки ингредиента: ' . $reason);
        }

        $this->attributes['check_reason'] = $reason;
    }

    /**
     * @param $reason
     * @throws InvalidAttributeException
     */
    public function setCheckReasonAttribute($reason)
    {
        $this->setCheckReason($reason);
    }
}
