<?php

namespace App\Models;

use App\Exceptions\Eloquent\InvalidAttributeException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    public const RECIPE_CREATED = 'recipe-created';
    public const RECIPE_UPDATED = 'recipe-updated';
    public const INGREDIENT_UPDATED = 'ingredient-updated';
    public const INGREDIENT_DECLINED = 'ingredient-declined';

    protected $appends = [
        'is_checked',
        'image_url',
        'image_thumb_url',
        'calories_per_hundred',
    ];

    protected $casts = [
        'is_vegan'      => 'boolean',
        'is_vegetarian' => 'boolean',
        'is_declined'   => 'boolean',
        // Общий вес или объем блюда: мл, гр. Мл для коктейлей
        'total_amount'  => 'decimal:2',
        // стоимость на все порции. стоимоть на порцию можно узнать поделив total_cost на portions_amount
        'total_cost'    => 'decimal:2',
        'portion_cost'  => 'decimal:2',
        // калрийность, бжу рассчитываются на 100 грамм
        'calories'      => 'decimal:2',
        'proteins'      => 'decimal:2',
        'fats'          => 'decimal:2',
        'carbohydrates' => 'decimal:2',
    ];

    protected $dates = [
        'checked_at',
        'ingredients_cost_updated_at',
    ];

    public function category()
    {
        return $this->belongsTo(RecipeCategory::class, 'category_id');
    }

    public function ingredients()
    {
        return $this
            ->belongsToMany(Ingredient::class, 'ingredient_recipe', 'recipe_id', 'ingredient_id')
            ->withPivot(['recipe_id', 'ingredient_id', 'amount']);
    }

    public function cookingTypes()
    {
        return $this->belongsToMany(CookingType::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function getCaloriesPerHundredAttribute()
    {
        $caloriesPer100 = null;
        if ($this->total_amount > 0 && $this->calories) {
            $caloriesPer100 = round($this->calories / $this->total_amount * 100, 2);
        }

        return $caloriesPer100;
    }

    public function getImageUrlAttribute()
    {
        return $this->image ?
            config('app.url') . '/' . config('filesystems.disks.recipes.root') . '/' . $this->image :
            null;
    }

    public function getImageThumbUrlAttribute()
    {
        return $this->image ?
            config('app.url') . '/' . config('filesystems.disks.recipes-thumb.root') . '/' . $this->image :
            null;
    }

    public function getIsCheckedAttribute()
    {
        if (!array_key_exists('checked_at', $this->attributes)) {
            return null;
        }
        return !is_null($this->attributes['checked_at']) || !is_null($this->checked_by);
    }

    // todo функция для установки статуса проверенности. ставит того, кто проверил либо статус, почему требует проверки
    // public function setChecked($checkerId, $reasonIfUnchecked = '') {}

    // public function scopeChecked() {}

    /**
     * @param string/null $reason
     * @throws InvalidAttributeException
     */
    public function setCheckReason($reason)
    {
        $allowedReasons = [
            null,
            self::RECIPE_CREATED,
            self::RECIPE_UPDATED,
            self::INGREDIENT_UPDATED,
            self::INGREDIENT_DECLINED,
        ];
        if (!in_array($reason, $allowedReasons)) {
            throw new InvalidAttributeException('Неверная причина для проверки рецепта: ' . $reason);
        }


        if (in_array($this->getOriginal('check_reason'), [self::INGREDIENT_UPDATED, self::INGREDIENT_DECLINED])) {
            // Если исходной причиной проверки было изменение ингредиента, оставляем статус
            // модератор должен знать, что ингредиенты были изменены
            if (in_array($reason, [self::RECIPE_CREATED, self::RECIPE_UPDATED])) {
                return;
            }
        }
        $this->attributes['check_reason'] = $reason;
    }

    /**
     * @param $reason
     * @throws InvalidAttributeException
     */
    public function setCheckReasonAttribute($reason)
    {
        $this->setCheckReason($reason);
    }

    public function setTotalCostAttribute($val)
    {
        $this->attributes['total_cost'] = $val;
        if ($this->portions_amount) {
            $this->attributes['portion_cost'] = $val / $this->portions_amount;
        }
    }

    public function setPortionCostAttribute($val)
    {
        $this->attributes['portion_cost'] = $val;
        if ($this->portions_amount) {
            $this->attributes['total_cost'] = $val * $this->portions_amount;
        }
    }

    public function scopeChecked($query)
    {
        return $query->where(function ($innerQuery) {
            $innerQuery->whereNotNull('checked_by')
                ->orWhereNotNull('checked_at');
        });
    }

    public function scopeSimilarNames($query, $string)
    {
        $regex = '~[^a-zA-Zа-яА-Я0-9]+~iu';
        $request = '*' . preg_replace($regex, '*', $string) . '*';
        return $query->whereRaw(
            "MATCH(name) AGAINST(? IN BOOLEAN MODE)",
            [$request]
        );
    }

    public function calculateTotalCost()
    {
        if (!$this->relationLoaded('ingredients')) {
            $this->load('ingredients');
        }
        $cost = 0;
        foreach ($this->ingredients as $ingredient) {
            $cost += $ingredient->pivot->amount / $ingredient->min_amount_to_buy * $ingredient->avg_unit_cost;
        }

        return (float)$cost;
    }

    public function getActualVegStatus()
    {
        if (!$this->relationLoaded('ingredients')) {
            $this->load('ingredients');
        }
        $isVegan = true;
        $isVegetarian = true;
        foreach ($this->ingredients as $i) {
            if ($i->vegan_component === false) {
                $isVegan = false;
            }
            if ($i->vegetarian_component === false) {
                $isVegetarian = false;
            }
        }
        return (object)[
            'is_vegan'      => $isVegan,
            'is_vegetarian' => $isVegetarian,
        ];
    }

    // информация о том, обновилась ли стоимость блюда
    public function checkCostFieldUpdate()
    {
        if (!$this->relationLoaded('ingredients')) {
            $this->load('ingredients');
        }
        $updateRequired = true;
        $timestamp = null;
        foreach ($this->ingredients as $i) {
            if (!($i->cost_added_at instanceof Carbon)) {
                $updateRequired = false;
            } else {
                $timestamp = $i->cost_added_at->gt($timestamp) ? $i->cost_added_at : $timestamp;
            }
        }

        return (object)[
            'update_required' => $updateRequired,
            'ts'              => $timestamp,
        ];
    }

    // калорийность на весь вес блюда
    public function calculateCalories()
    {
        if (!$this->relationLoaded('ingredients')) {
            $this->load('ingredients');
        }
        $calories = 0;
        foreach ($this->ingredients as $ingredient) {
            switch ($ingredient->units) {
                case 'гр':
                case 'мл':
                    $calories += $ingredient->calories / 100 * $ingredient->pivot->amount;
                    break;
                case 'шт':
                    $calories += $ingredient->calories / 100 * $ingredient->avg_piece_weight * $ingredient->pivot->amount;
                    break;
                case 'кг':
                case 'л':
                    $calories += $ingredient->calories / 100 * $ingredient->pivot->amount * 1000;
                    break;
                default:
                    return false;
            }
        }
        return $calories;
    }

    // калорийность 100 грамм блюда
    public function calculateCaloriesPer100Gramm()
    {
        return $this->calculateCalories() / $this->calculateTotalAmount() * 100;
    }

    // Общий вес блюда
    public function calculateTotalAmount()
    {
        if (!$this->relationLoaded('ingredients')) {
            $this->load('ingredients');
        }
        $amount = 0;
        foreach ($this->ingredients as $ingredient) {
            switch ($ingredient->units) {
                case 'гр':
                case 'мл':
                    $amount += $ingredient->pivot->amount;
                    break;
                case 'шт':
                    $amount += $ingredient->avg_piece_weight * $ingredient->pivot->amount;
                    break;
                case 'кг':
                case 'л':
                    $amount += $ingredient->pivot->amount * 1000;
                    break;
                default:
                    return false;
            }
        }

        return (float)$amount;
    }

    // бжу на 100 грамм
    public function calculateComposition()
    {
        if (!$this->relationLoaded('ingredients')) {
            $this->load('ingredients');
        }
        $result = (object)[
            'proteins'      => (float)0,
            'fats'          => (float)0,
            'carbohydrates' => (float)0,
        ];
        foreach ($this->ingredients as $ingredient) {
            switch ($ingredient->units) {
                case 'гр':
                case 'мл':
                    $result->proteins += $ingredient->proteins / 100 * $ingredient->pivot->amount;
                    $result->fats += $ingredient->fats / 100 * $ingredient->pivot->amount;
                    $result->carbohydrates += $ingredient->carbohydrates / 100 * $ingredient->pivot->amount;
                    break;
                case 'шт':
                    $result->proteins += $ingredient->proteins / 100 * $ingredient->avg_piece_weight * $ingredient->pivot_amount;
                    $result->fats += $ingredient->fats / 100 * $ingredient->avg_piece_weight * $ingredient->pivot_amount;
                    $result->carbohydrates += $ingredient->carbohydrates / 100 * $ingredient->avg_piece_weight * $ingredient->pivot_amount;
                    break;
                case 'кг':
                case 'л':
                    $result->proteins += $ingredient->proteins / 100 * $ingredient->pivot->amount * 1000;
                    $result->fats += $ingredient->fats / 100 * $ingredient->pivot->amount * 1000;
                    $result->carbohydrates += $ingredient->carbohydrates / 100 * $ingredient->pivot->amount * 1000;
                    break;
                default:
                    return false;
            }
        }

        return $result;
    }
}
