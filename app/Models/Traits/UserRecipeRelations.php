<?php namespace App\Models\Traits;

use App\Models\Recipe;

/**
 * User: Tantacula
 * Date: 10.07.2019
 * Time: 5:08
 */
trait UserRecipeRelations
{
    public function favoriteRecipes()
    {
        return $this->belongsToMany(Recipe::class, 'favorite_recipes', 'user_id', 'recipe_id');
    }

    public function postponedRecipes()
    {
        return $this->belongsToMany(Recipe::class, 'postponed_recipes', 'user_id', 'recipe_id');
    }

    public function dislikedRecipes()
    {
        return $this->belongsToMany(Recipe::class, 'disliked_recipes', 'user_id', 'recipe_id');
    }
}