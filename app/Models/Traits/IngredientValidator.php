<?php namespace App\Models\Traits;

//use Illuminate\Validation\Rule;

/**
 * User: Tantacula
 * Date: 19.07.2019
 * Time: 15:12
 */
trait IngredientValidator
{
    public $requiredParamsForUserAddedIngredient = [
        'name',
        'units',
    ];

    public static $requiredParamsForCheckedIngredient = [
        'name',
        'units',
        'is_loose',
        'amount_in_glass_200ml',
        'min_amount_to_buy',
        'comment',
        'calories',
        'proteins',
        'fats',
        'carbohydrates',
        'vegan_component',
        'vegetarian_component',
        'avg_unit_cost',
        'avg_piece_weight',
    ];

    public function getRequiredParamsForUserAddedIngredient()
    {
        return $this->requiredParamsForUserAddedIngredient;
    }

    public function getRequiredParamsForCheckedIngredient()
    {
        return static::$requiredParamsForCheckedIngredient;
    }

    public function getValidatorForUser(array $data)
    {
        $rules = [
            'name'  => 'required',
            'units' => 'required|in:' . implode(',', config('ingredients.units')),
        ];
        return \Validator::make($data, $rules);
    }

    // написать юнит тесты для валидаторов
    public function getValidatorForModerator(array $data)
    {
        $rules = [
            'name'                  => 'required|string',
            'units'                 => 'required|in:' . implode(',', config('ingredients.units')),
            'is_loose'              => 'required|boolean',
            'amount_in_glass_200ml' => 'nullable|integer|required_if:is_loose,true',
            'min_amount_to_buy'     => 'required|numeric',
            'comment'               => 'nullable|string', //
            'calories'              => 'required|between:0,999.99',
            'proteins'              => 'required|between:0,99.99',
            'fats'                  => 'required|between:0,99.99',
            'carbohydrates'         => 'required|between:0,99.99',
            'vegan_component'       => 'required|boolean',
            'vegetarian_component'  => 'required|boolean',
            'avg_unit_cost'         => 'required|numeric',
            'avg_piece_weight'      => 'nullable|integer|required_if:units,шт',
        ];
        return \Validator::make($data, $rules);
    }

    /**
     * Метод проверяет, заполнены ли все данные, которые требуются для заданной роли пользоватля
     * @return object
     */
    public function checkModelFilling($role = 'moderator')
    {
        $emptyParam = null;

        // модератор должен заполнять все поля,
        // простой пользозватель и админ могут заполнить лишь название и единицы измерения
        if ($role === 'moderator') {
            $validator = $this->getValidatorForModerator($this->toArray());
        } else {
            $validator = $this->getValidatorForUser($this->toArray());
        }

        $isFilled = $validator->passes();
        if (!$isFilled) {
            $failedRules = $validator->failed();
            $failedItems = array_keys($failedRules);
            $emptyParam = $failedItems[0];
        }


        // модератор должен заполнять все полностью
//        if ($role === 'moderator') {
//            foreach (self::$requiredParamsForCheckedIngredient as $item) {
//                // кроме этих полей все остальные должны быть обязателньо !null
//                if (
//                    $item !== 'comment' &&
//                    $item !== 'avg_piece_weight' &&
//                    $item !== 'amount_in_glass_200ml'
//                ) {
//                    if (is_null($this->{$item}) || $this->{$item} === '') {
//                        $isFilled = false;
//                        $emptyParam = $item;
//                    }
//                }
//                if ($item === 'avg_piece_weight') {
//                    if (
//                        $this->units === 'шт' && (is_null($this->{$item}) || $this->{$item} === '')
//                    ) {
//                        $isFilled = false;
//                        $emptyParam = $item;
//                    }
//                }
//                if ($item === 'amount_in_glass_200ml' && $this->is_loose) {
//                    $isFilled = false;
//                    $emptyParam = $item;
//                }
//            }
//            return (object)compact('isFilled', 'emptyParam');
//        }
        // else if role === user or admin - админу положено что угодно, рядовому юзеру можно добавлять лишь название и единицы измерения
//        foreach (self::$requiredParamsForUserAddedIngredient as $item) {
//            if (is_null($this->{$item}) || $this->{$item} === '') {
//                $isFilled = false;
//                $emptyParam = $item;
//            }
//        }
        return (object)compact('isFilled', 'emptyParam');
    }

    // можно ли проставлять флаг checked
    public function isFilledForApprove(): bool
    {
        return $this->checkModelFilling('moderator')->isFilled;
    }
}