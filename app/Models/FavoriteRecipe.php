<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FavoriteRecipe extends Model
{
    protected $fillable = [
        'recipe_id',
        'user_id',
    ];

    public function recipe()
    {
        return $this->hasOne(Recipe::class);
    }
}
