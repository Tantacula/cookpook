<?php

namespace App\Http\Resources;

use App\Models\CookingType;
use App\Models\Ingredient;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class RecipeUserResource
 * @package App\Http\Resources
 * @mixin \App\Models\Recipe
 */
class RecipeUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $ingredients = [];
        foreach ($this->ingredients as $i) {
            $ingredients[] = [
                'id'     => $i->id,
                'name'   => $i->name,
                'units'  => $i->units,
                'amount' => preg_replace('~(\.00|0|\.|\.0)$~', '', $i->pivot->amount),
            ];
        }
        $cookingTypes = [];
        foreach ($this->cookingTypes as $c) {
            $cookingTypes[] = [
                'id'   => $c->id,
                'name' => $c->name,
            ];
        }
        $category = null;
        if ($this->category) {
            $category = [
                'id'   => $this->category->id,
                'name' => $this->category->name,
            ];
        }

        return [
            'id'                   => $this->id,
            'name'                 => $this->name,
            'slug'                 => $this->slug,
            'article_text'         => $this->article_text,
            'image_url'            => $this->image_url,
            'image_thumb_url'      => $this->image_thumb_url,
            'calories'             => $this->calories,
            'calories_per_hundred' => $this->calories_per_hundred,
            'portion_cost'         => round($this->portion_cost),
            'portions_amount'      => $this->portions_amount,
            'cooking_time_minutes' => $this->cooking_time_minutes,
            'is_vegan'             => $this->is_vegan,
            'is_vegetarian'        => $this->is_vegetarian,
            'ingredients'          => $ingredients,
            'cookingTypes'         => $cookingTypes,
            'category'             => $category,
            'author_id'            => $this->author_id,
            'category_id'          => $this->category_id,
            'is_published'         => $this->is_published,
            'total_cost'           => $this->total_cost,
            'marks_amount'         => $this->marks_amount,
            'is_checked'           => $this->is_checked,
        ];
    }
}
