<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class RecipeAdminResource
 * @package App\Http\Resources
 * @mixin \App\Models\Recipe
 */
class RecipeAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $ingredients = [];
        foreach ($this->ingredients as $i) {
            $ingredients[] = [
                'id'     => $i->id,
                'amount' => preg_replace('~(\.00|0|\.|\.0)$~', '', $i->pivot->amount),
            ];
        }
        $cookingTypes = [];
        foreach ($this->cookingTypes as $c) {
            $cookingTypes[] = [
                'id'   => $c->id,
                'name' => $c->name,
            ];
        }
        return [
            'id'                   => $this->id,
            'author_id'            => $this->author_id,
            'author'               => [
                'id'   => $this->author_id,
                'name' => $this->author ? $this->author->name : null,
            ],
            'name'                 => $this->name,
            'category_id'          => $this->category_id,
            'slug'                 => $this->slug,
            'article_text'         => $this->article_text,
            'image_url'            => $this->image_url,
            'image_thumb_url'      => $this->image_thumb_url,
            'calories'             => $this->calories,
            'calories_per_hundred' => $this->calories_per_hundred,
            'is_published'         => $this->is_published,
            'portion_cost'         => round($this->portion_cost),
            'portions_amount'      => $this->portions_amount,
            'total_cost'           => $this->total_cost,
            'cost_calculated_at'   => $this->cost_calculated_at,
            'cooking_time_minutes' => $this->cooking_time_minutes,
            'is_vegan'             => $this->is_vegan,
            'is_vegetarian'        => $this->is_vegetarian,
            'marks_amount'         => $this->marks_amount,
            'checked_at'           => $this->checked_at,
            'checked_by'           => $this->checked_by,
            'is_checked'           => $this->is_checked,
            'ingredients'          => $ingredients,
            'cookingTypes'         => $cookingTypes,
            'created_at'           => $this->created_at,
            'updated_at'           => $this->updated_at,
        ];
    }
}
