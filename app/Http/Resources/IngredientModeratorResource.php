<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class IngredientModeratorResource
 * @package App\Http\Resources
 * @mixin \App\Models\Ingredient
 */
class IngredientModeratorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                   => $this->id,
            'name'                 => $this->name,
            'units'                => $this->units,
            'is_loose'             => $this->is_loose,
            'amouny_in_glass_200ml' => $this->amount_in_glass_200ml,
            'min_amount_to_buy'    => $this->removeTailingZeros($this->min_amount_to_buy),
            'calories'             => $this->calories,
            'proteins'             => $this->proteins,
            'fats'                 => $this->fats,
            'carbohydrates'        => $this->carbohydrates,
            'vegan_component'      => $this->vegan_component,
            'vegetarian_component' => $this->vegetarian_component,
            'avg_unit_cost'        => $this->removeTailingZeros($this->avg_unit_cost),
            'avg_piece_weight'     => $this->avg_piece_weight,
            'created_by'           => $this->created_by,
            //'checked_at'           => $this->checked_at,
            //'checked_by'           => $this->checked_by,
            'is_checked'           => $this->is_checked,
            'check_reason'         => $this->check_reason,
        ];
    }

    private function removeTailingZeros($number)
    {
        return preg_replace('~(\.00|0|\.|\.0)$~', '', $number);
    }
}
