<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRecipeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ingredients.*.id'     => 'required|exists:ingredients',
            'ingredients.*.amount' => 'required|numeric',
            'cookingTypes.*'       => 'required|exists:cooking_types,id',
            'name'                 => 'required|string',
            'category_id'          => 'required|exists:recipe_categories,id',
            'article_text'         => 'required|string',
            'image'                => 'required|image|dimensions:min_width=690,min_height=320',
            'portions_amount'      => 'required|numeric',
            'cooking_time_minutes' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'image.dimensions' => 'Изображение должно иметь размер 690 на 320 пикселей.',
        ];
    }
}
