<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (!$request->user() || !$request->user()->hasRole($role)) {
            // throw new AuthenticationException()...
            return response()->json([
                'error' => ($request->user() ? $request->user()->role : 'user') . ' is not authenticated for this action.',
            ], 403);
        }
        return $next($request);
    }
}
