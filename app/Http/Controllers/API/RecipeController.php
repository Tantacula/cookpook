<?php

namespace App\Http\Controllers\API;

use App\Events\NewRecipeWasAdded;
use App\Events\RecipeIngredientsWasUpdated;
use App\Http\Requests\DeclineRecipeRequest;
use App\Http\Requests\FindSimilarRecipeRequest;
use App\Http\Resources\RecipeAdminResource;
use App\Http\Resources\RecipeUserResource;
use App\Jobs\ImageProcessing\DeleteRecipeImage;
use App\Jobs\ImageProcessing\SaveRecipeImage;
use App\Models\DislikedRecipe;
use App\Models\FavoriteRecipe;
use App\Models\PostponedRecipe;
use App\Models\Recipe;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class RecipeController extends Controller
{

    private $recipe;

    public function __construct(Recipe $recipe)
    {
        $this->middleware('role:admin')->only(['destroy', 'list']);
        $this->recipe = $recipe;
    }


    /**
     * Display a listing of the resource.
     * На данный момент метод требуется лишь админу, поэтому в мидлваре проверка этой роли
     *
     * @return \Illuminate\Http\Response|JsonResource
     */
    public function list()
    {
        $recipes = $this->recipe->with('category', 'ingredients', 'cookingTypes', 'author')
            ->orderBy('id', 'desc')
            ->paginate(30);

        switch (\Auth::user()->role) {
            case 'admin':
                return RecipeAdminResource::collection($recipes);
            default:
                return RecipeUserResource::collection($recipes);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response|JsonResource
     */
    public function getUserOwnRecipes()
    {
        $recipes = $this->recipe->with('category', 'ingredients', 'cookingTypes')
            ->where('author_id', \Auth::user()->id)
            ->orderBy('id', 'desc')
            ->paginate(15);

        return RecipeUserResource::collection($recipes);
    }

    public function findSimilarRecipes(FindSimilarRecipeRequest $request)
    {
        $recipes = $this->recipe->similarNames($request->name)
            ->take(30)
            ->get(['id', 'name']);

        return compact('recipes');
    }

    public function unapproved()
    {
        $recipes = $this->recipe->with('category', 'ingredients', 'cookingTypes')
            ->whereNull('checked_at')
            ->whereNull('is_declined')
            ->orderBy('id', 'asc')
            ->paginate(15);

        return RecipeUserResource::collection($recipes);
    }

    public function search(Request $request)
    {
        $recipesQuery = $this->recipe->with('category', 'ingredients', 'cookingTypes')
            ->whereNull('is_declined');

        if ((bool)$request->get('vegan')) {
            $recipesQuery->where('is_vegan', true);
        } else {
            if ((bool)$request->get('vegetarian')) {
                $recipesQuery->where('is_vegetarian', true);
            }
        }
        if ($request->get('onlyChecked')) {
            $recipesQuery->checked();
        }

        $anyCost = (bool)$request->get('anyPortionCost');
        $minCost = $request->get('minPortionCost');
        $maxCost = $request->get('maxPortionCost');
        if (!$anyCost && ($minCost || $maxCost)) {
            if (!$minCost) {
                $recipesQuery->where('portion_cost', '<=', $maxCost);
            } else {
                if (!$maxCost) {
                    $recipesQuery->where('portion_cost', '>=', $minCost);
                } else {
                    $recipesQuery->whereBetween('portion_cost', [$minCost, $maxCost]);
                }
            }
        }
        if ($request->get('cookingTime')) {
            $recipesQuery->where('cooking_time_minutes', '<=', $request->get('cookingTime'));
        }

        if (\Auth::user()) {
            $recipesQuery->whereNotExists(function ($query) {
                $query->select('user_id', 'recipe_id')
                    ->from('disliked_recipes')
                    ->where('user_id', \Auth::user()->id)
                    ->whereRaw('disliked_recipes.recipe_id = recipes.id');
            });
        }

        // todo пока не хайлоад, потом использовать https://ruhighload.com/%d0%9e%d0%bf%d1%82%d0%b8%d0%bc%d0%b8%d0%b7%d0%b0%d1%86%d0%b8%d1%8f+order+by+rand%28%29 либо кэширование
        $recipesQuery->inRandomOrder();
        $recipesQuery->take(6);

        $recipes = RecipeUserResource::collection($recipesQuery->get());

        return compact('recipes');
    }

    public function withTotalCostUnder($amount)
    {
        $recipes = $this->recipe->with('category', 'ingredients', 'cookingTypes')
            ->where('total_cost', '<=', $amount)
            ->orderBy('id', 'desc')
            ->paginate(15);

        return RecipeUserResource::collection($recipes);
    }

    public function withPortionCostUnder($amount)
    {
        $recipes = $this->recipe->with('category', 'ingredients', 'cookingTypes')
            ->where('portion_cost', '<=', $amount)
            ->orderBy('id', 'desc')
            ->paginate(15);

        return RecipeUserResource::collection($recipes);
    }

    public function getRecipeById($id)
    {
        // пока используется для получения инфы о рецепте на пользовательской карточке. Можно отправлять данные в зависимости от роли
        return RecipeUserResource::make($this->recipe->find($id));
    }

    private function recipeExists($params)
    {
        return !is_null($this->recipe->where('name', $params['name'])->first());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response|array
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $ingredients = $request->ingredients;
        if (!is_array($ingredients) || count($ingredients) === 0) {
            return response(['error' => 'Рецепт должен включать в себя ингредиенты.'],
                422);
        }
        $cookingTypes = $request->cookingTypes;
        if (count($cookingTypes) === 0) {
            return response(['error' => 'Выберите тип приготовления.'],
                422);
        }
        if ($this->recipeExists(['name' => $request->get('name')])) {
            return response(['error' => 'Рецепт с таким названием уже был добавлен.'], 422);
        }

        \DB::beginTransaction();
        try {
            $recipe = new Recipe();
            $recipe->author_id = \Auth::user()->id;
            $recipe->name = $request->name;
            $recipe->category_id = $request->category_id;
            $recipe->slug = time() . '-' . \Auth::user()->id . Str::slug($request->name);
            $recipe->article_text = $request->article_text;
            $recipe->image = $this->saveImage($request->file('image'), \Auth::user()->id);
            $recipe->is_published = \Auth::user()->can_approve_recipes;
            $recipe->portions_amount = $request->portions_amount;
            $recipe->cooking_time_minutes = $request->cooking_time_minutes;
            $recipe->marks_amount = 0;

            if (\Auth::user()->can_approve_recipes) {
                $recipe->checked_at = Carbon::now();
                $recipe->checked_by = \Auth::user()->id;
                $recipe->setCheckReason(null);
            } else {
                $recipe->checked_at = null;
                $recipe->checked_by = null;
                $recipe->setCheckReason(Recipe::RECIPE_CREATED);
            }

            $recipe->save();

            $now = Carbon::now();
            $ingredientPivots = [];
            foreach ($ingredients as $item) {
                $ingredientPivots[] = [
                    'recipe_id'     => $recipe->id,
                    'ingredient_id' => $item['id'],
                    'amount'        => $item['amount'],
                    'created_at'    => $now,
                    'updated_at'    => $now,
                ];
            }
            \DB::table('ingredient_recipe')->insert($ingredientPivots);

            $cookingTypePivots = [];
            foreach ($cookingTypes as $type) {
                $cookingTypePivots[] = [
                    'recipe_id'       => $recipe->id,
                    'cooking_type_id' => $type,
                    'created_at'      => $now,
                    'updated_at'      => $now,
                ];
            }
            \DB::table('cooking_type_recipe')->insert($cookingTypePivots);

            event(new NewRecipeWasAdded($recipe->id));

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
            // \Log::error($e->getTraceAsString());

            // return response(['error' => "Ошибка. {$e->getMessage()}"], 500);
        }

        return [
            'message' => [
                'text'   => 'Рецепт успешно добавлен.',
                'status' => 'success',
            ],
        ];

    }

    private function saveImage(UploadedFile $file, $userId)
    {
        $filename = time();
        dispatch_now(new SaveRecipeImage($file, $filename, $userId));

        // author_id is nullable, thats why we include its ID into path
        return $userId . '/' . $filename . '.jpg';
    }

    // todo перенести в eloquentServiceProvider на обновление данных об изображении
    private function deleteImage($imgSrc = '')
    {
        dispatch_now(new DeleteRecipeImage($imgSrc));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Recipe $recipe
     * @return \Illuminate\Http\Response|array
     * @throws \Exception
     */
    public function update(Request $request, Recipe $recipe)
    {
        if ($recipe->is_checked && !\Auth::user()->can_approve_recipes) {
            return response(['error' => 'Вы не можете отредактировать этот рецепт.'],
                422);
        }
        if ($recipe->author_id !== \Auth::user()->id && !\Auth::user()->can_approve_recipes) {
            return response(['error' => 'Вы не можете отредактировать чужой рецепт.'],
                422);
        }

        $recipe->load('ingredients');

        \DB::beginTransaction();
        try {
            $recipe->name = $request->name ?: $recipe->name;
            $recipe->category_id = $request->category_id ?: $recipe->category_id;
            $recipe->slug = ($request->name === $recipe->name) ? $recipe->slug : time() . '-' . \Auth::user()->id . Str::slug($request->name);
            $recipe->article_text = $request->article_text ?: $recipe->article_text;
            if ($request->hasFile('image')) {
                $this->deleteImage($recipe->image);
                $recipe->image = $this->saveImage($request->file('image'), \Auth::user()->id);
            }

            $recipe->portions_amount = $request->portions_amount ?: $recipe->portions_amount;
            $recipe->cooking_time_minutes = $request->cooking_time_minutes ?: $recipe->cooking_time_minutes;

            if (\Auth::user()->can_approve_recipes) {
                $recipe->checked_at = Carbon::now();
                $recipe->checked_by = \Auth::user()->id;
                // todo: это можно вынести в сервис провайдер, чтобы причина проверки автоматом снималась после установки checked_at
                $recipe->setCheckReason(null);
            } else {
                $recipe->checked_at = null;
                $recipe->checked_by = null;
                $recipe->setCheckReason(Recipe::RECIPE_UPDATED);
            }

            $recipe->save();

            $now = Carbon::now();

            $ingredients = $request->ingredients;
            $cookingTypes = $request->cookingTypes;

            $ingredientsWasUpdated = $this->ingredientsChanged($recipe, $ingredients);

            if (count($ingredients) > 0 && $ingredientsWasUpdated) {
                \DB::table('ingredient_recipe')->where('recipe_id', $recipe->id)->delete();
                $ingredientPivots = [];
                foreach ($ingredients as $item) {
                    $ingredientPivots[] = [
                        'recipe_id'     => $recipe->id,
                        'ingredient_id' => $item['id'],
                        'amount'        => $item['amount'],
                        'created_at'    => $now,
                        'updated_at'    => $now,
                    ];
                }
                \DB::table('ingredient_recipe')->insert($ingredientPivots);
            }

            if (count($cookingTypes) > 0) {
                \DB::table('cooking_type_recipe')->where('recipe_id', $recipe->id)->delete();
                $cookingTypePivots = [];
                foreach ($cookingTypes as $type) {
                    $cookingTypePivots[] = [
                        'recipe_id'       => $recipe->id,
                        'cooking_type_id' => $type,
                        'created_at'      => $now,
                        'updated_at'      => $now,
                    ];
                }
                \DB::table('cooking_type_recipe')->insert($cookingTypePivots);
            }

            if ($ingredientsWasUpdated) {
                event(new RecipeIngredientsWasUpdated($recipe->id));
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getTraceAsString());

            return response(['error' => "Ошибка. {$e->getMessage()}"], 500);
        }

        // todo проверить авторство либо роль
        // опции checked_at checked_by
        // если роль не позволяет редактировать, а ресурс уже проверен, запрещать
        // если есть пустые - удалить их. непустые обновить, удалить старую картинку, вернуть recipe => RecipeUserResource, message

        return [
            'message' => [
                'text'   => 'Рецепт успешно изменен.',
                'status' => 'success',
            ],
            'recipe'  => RecipeUserResource::make($recipe),
        ];
    }

    private function ingredientsChanged(Recipe $recipe, array $newIngredients): bool
    {
        if (count($recipe->ingredients) !== count($newIngredients)) {
            return true;
        }
        $originalIngredients = [];
        $recipe->ingredients->map(function ($i) use (&$originalIngredients) {
            $originalIngredients[$i->id] = [
                'id'     => $i->id,
                'amount' => (float)$i->pivot->amount,
            ];
        });
        $identical = true;
        foreach ($newIngredients as $newIngredient) {
            if (array_key_exists($newIngredient['id'], $originalIngredients)) {
                $original = $originalIngredients[$newIngredient['id']];
                if ($original['amount'] !== (float)$newIngredient['amount']) {
                    $identical = false;
                    break;
                }
            } else {
                $identical = false;
                break;
            }
        }

        return !$identical;
    }

    public function decline(DeclineRecipeRequest $request, $id)
    {
        $recipe = $this->recipe->findOrFail($id);
        if (!\Auth::user()->can_approve_recipes) {
            return response(['error' => 'Вы не можете отклонять рецепты.'], 422);
        }
        $recipe->is_declined = true;
        $recipe->decline_reason = $request->reason;
        $recipe->declined_by = \Auth::user()->id;
        $recipe->save();

        return [
            'message' => [
                'text'   => 'Рецепт был отклонен.',
                'status' => 'success',
            ],
        ];
    }

    public function getFavorites()
    {
        return RecipeUserResource::collection(
            \Auth::user()->favoriteRecipes()
                ->with('category', 'ingredients', 'cookingTypes')
                ->orderBy('id', 'desc')
                ->paginate(15));
    }

    public function getRecipePreferencesData()
    {
        $favoriteIds = $this->getFavoriteIds()['ids'];
        $postponedIds = $this->getPostponedIds()['ids'];
        return compact('favoriteIds', 'postponedIds');
    }

    private function getFavoriteIds()
    {
        $ids = FavoriteRecipe::where('user_id', \Auth::user()->id)->pluck('recipe_id');
        return compact('ids');
    }

    public function addToFavorite(Request $request)
    {
        $recipeId = $request->get('recipe_id');
        FavoriteRecipe::firstOrCreate(
            [
                'recipe_id' => $recipeId,
                'user_id'   => \Auth::user()->id,
            ]
        );

        return [
            'recipe_id' => $recipeId,
        ];
    }

    public function removeFromFavorite(Request $request)
    {
        $recipeId = $request->get('recipe_id');
        FavoriteRecipe::where('recipe_id', $recipeId)
            ->where('user_id', \Auth::user()->id)
            ->delete();

        return [
            'recipe_id' => $recipeId,
        ];
    }

    public function getPostponed()
    {
        return RecipeUserResource::collection(
            \Auth::user()->postponedRecipes()
                ->with('category', 'ingredients', 'cookingTypes')
                ->orderBy('id', 'desc')
                ->paginate(15));
    }

    public function getPostponedIds()
    {
        $ids = PostponedRecipe::where('user_id', \Auth::user()->id)->pluck('recipe_id');
        return compact('ids');
    }

    public function addToPostponed(Request $request)
    {
        $recipeId = $request->get('recipe_id');
        PostponedRecipe::firstOrCreate(
            [
                'recipe_id' => $recipeId,
                'user_id'   => \Auth::user()->id,
            ]
        );

        return [
            'recipe_id' => $recipeId,
        ];
    }

    public function removeFromPostponed(Request $request)
    {
        $recipeId = $request->get('recipe_id');
        PostponedRecipe::where('recipe_id', $recipeId)
            ->where('user_id', \Auth::user()->id)
            ->delete();

        return [
            'recipe_id' => $recipeId,
        ];
    }

    public function dislikeRecipe(Request $request)
    {
        $recipeId = $request->get('recipe_id');
        DislikedRecipe::firstOrCreate(
            [
                'recipe_id' => $recipeId,
                'user_id'   => \Auth::user()->id,
            ]
        );

        return [
            'message' => [
                'text'   => 'Рецепт был отмечен как непоравившийся.',
                'status' => 'success',
            ],
        ];
    }

    public function destroy($id)
    {
        $this->recipe->destroy($id);

        return [
            'message' => [
                'text'   => 'Рецепт успешно удален.',
                'status' => 'success',
            ],
        ];
    }
}
