<?php

namespace App\Http\Controllers\API;

use App\Models\CookingType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CookingTypeController extends Controller
{
    /**
     * @var CookingType
     */
    private $cookingType;

    /**
     * CookingTypeController constructor.
     * @param CookingType $cookingType
     */
    public function __construct(CookingType $cookingType)
    {
        $this->cookingType = $cookingType;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cookingTypes = $this->cookingType->all();
        $other = $cookingTypes->first(function ($item) {
            return $item->slug === 'other';
        });
        $cookingTypes = $cookingTypes->reject(function ($item) {
            return $item->slug === 'other';
        });
        $cookingTypes->push($other);
        $cookingTypes = $cookingTypes->values();

        return compact('cookingTypes');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
