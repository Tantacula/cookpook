<?php

namespace App\Http\Controllers\API;

use App\Models\RecipeCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecipeCategoryController extends Controller
{
    /**
     * @var RecipeCategory
     */
    public $recipeCategory;

    /**
     * RecipeCategoryController constructor.
     * @param RecipeCategory $recipeCategory
     */
    public function __construct(RecipeCategory $recipeCategory)
    {
        $this->recipeCategory = $recipeCategory;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response|array
     */
    public function index(): array
    {
        $categories = $this->recipeCategory->all();
        return compact('categories');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RecipeCategory $recipeCategory
     * @return \Illuminate\Http\Response
     */
    public function show(RecipeCategory $recipeCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\RecipeCategory $recipeCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RecipeCategory $recipeCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RecipeCategory $recipeCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecipeCategory $recipeCategory)
    {
        //
    }
}
