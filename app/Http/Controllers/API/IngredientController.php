<?php

namespace App\Http\Controllers\API;

use App\Exceptions\IngredientExistsException;
use App\Exceptions\IngredientValidatorException;
use App\Exceptions\ResourceAccessException;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateIngredientRequest;
use App\Http\Requests\DeclineIngredientRequest;
use App\Http\Resources\IngredientModeratorResource;
use App\Jobs\Ingredient\CreateIngredient;
use App\Jobs\Ingredient\UpdateIngredient;
use App\Models\Ingredient;
use App\Models\Recipe;
use Carbon\Carbon;
use Illuminate\Http\Request;

class IngredientController extends Controller
{
    /**
     * @var array
     */
    private $paramsToFill;

    /**
     * @var Ingredient
     */
    private $ingredient;

    /**
     * IngredientController constructor.
     * @param Ingredient $ingredient
     */
    public function __construct(Ingredient $ingredient)
    {
        $this->paramsToFill = $ingredient->getRequiredParamsForCheckedIngredient();
        $this->ingredient = $ingredient;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response|array
     */
    public function index(): array
    {
        // todo: разделить на ресурсы для модераторов и простых пользователей
        $ingredients = $this->ingredient->all();
        return compact('ingredients');
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function unapproved()
    {
        $ingredients = $this->ingredient->whereNull('checked_at')
            ->whereNull('is_declined')
            ->orderBy('id', 'asc')
            ->paginate(15);

        return IngredientModeratorResource::collection($ingredients);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateIngredientRequest $request
     * @return \Illuminate\Http\Response|array
     */
    public function store(CreateIngredientRequest $request): array
    {
        try {
            $i = dispatch_now(new CreateIngredient($request->all(), \Auth::user()));
        } catch (IngredientExistsException $e) {
            return response(['error' => $e->getMessage()], 422);
        } catch (IngredientValidatorException $e) {
            return response(['error' => $e->getMessage()], 422);
        }

        return ['ingredient' => $i];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response| array
     * @throws \Exception
     * @throws ResourceAccessException
     */
    public function update(Request $request, $id): array
    {
        try {
            \DB::beginTransaction();
            try {
                $ingredient = dispatch_now(
                    new UpdateIngredient($id, $request->all(), \Auth::user())
                );
                \DB::commit();
            } catch (\Exception $e) {
                \DB::rollBack();
                throw $e;
            }
        } catch (ResourceAccessException $e) {
            return response(['error' => $e->getMessage()], 422);
        }

        // todo: ingredient должен быть отдан ресурсом (или модераторским ресурсом)
        return ['status' => 'ok', 'ingredient' => $ingredient];
    }

    public function decline(DeclineIngredientRequest $request, Ingredient $ingredient)
    {
        if (!\Auth::user()->can_approve_ingredients) {
            return response(['error' => 'Вы не можете отклонять ингредиенты.'], 422);
        }
        $ingredient->decline($request->reason, \Auth::user()->id);
        $ingredient->save();

        return [
            'message' => [
                'text'   => 'Ингредиент был отклонен.',
                'status' => 'success',
            ],
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ingredient $ingredient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ingredient $ingredient)
    {
        //
    }
}
