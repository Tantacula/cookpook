<?php

namespace App\Listeners;

use App\Contracts\Events\RecipeEventContract;
use App\Jobs\Recipe\CalculateRecipeComposition;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecipeIngredientsUpdateHandler implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     * @return void
     */
    public function handle(RecipeEventContract $event)
    {
        dispatch_now(new CalculateRecipeComposition($event->getRecipeId()));
    }
}
