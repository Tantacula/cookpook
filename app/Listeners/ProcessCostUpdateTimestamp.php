<?php

namespace App\Listeners;

use App\Events\IngredientCostWasUpdated;
use App\Models\Ingredient;
use App\Models\Recipe;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessCostUpdateTimestamp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(IngredientCostWasUpdated $e)
    {
        $ingredient = Ingredient::find($e->getIngredientId());
        if (!$ingredient) {
            return;
        }
        $recipes = $ingredient->recipes;
        if (count($recipes) === 0) {
            return;
        }
        $ids = $recipes->pluck('id');
        \DB::table((new Recipe())->getTable())
            ->whereIn('id', $ids)
            ->update([
                'ingredients_cost_updated_at' => $e->getCostUpdatedAt(),
            ]);

    }
}
