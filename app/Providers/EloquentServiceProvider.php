<?php

namespace App\Providers;

use App\Events\IngredientCostWasUpdated;
use App\Jobs\ImageProcessing\DeleteRecipeImage;
use App\Jobs\Ingredient\RecalculateIngredientAmountsAfterUnitsTransform;
use App\Jobs\Recipe\ProcessRecipesWithUpdatedIngredients as ResetRecipeCheckFlag;
use App\Jobs\Recipe\RecheckRecipesForVegetarianProperties;
use App\Jobs\Recipe\RecheckRecipesForVegetarianPropertiesByIngredientId;
use App\Jobs\UpdateSitemap;
use App\Models\Ingredient;
use App\Models\Recipe;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class EloquentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootRecipeEvents();
        $this->bootIngredientEvents();
    }

    private function bootRecipeEvents()
    {
        Recipe::saving(function (Recipe $model) {
            if ($model->is_checked) {
                $model->check_reason = null;
            }
            if (
                $model->getOriginal('total_cost') !== $model->total_cost ||
                $model->getOriginal('portion_cost') !== $model->portion_cost
            ) {
                $model->cost_calculated_at = Carbon::now();
            }
        });

        Recipe::saved(function (Recipe $model) {
            \Log::channel('recipes')->info($model);
        });

        Recipe::created(function (Recipe $model) {
            dispatch(new UpdateSitemap());
        });

        Recipe::deleted(function (Recipe $model) {
            dispatch(new DeleteRecipeImage($model->image));
        });
    }

    private function bootIngredientEvents()
    {
        Ingredient::saving(function (Ingredient $model) {
            if ($model->is_checked) {
                $model->check_reason = null;
            }
            if ($model->getOriginal('avg_unit_cost') !== $model->avg_unit_cost || $model->avg_unit_cost) {
                $model->cost_added_at = Carbon::now();
            }
        });

        Ingredient::saved(function (Ingredient $model) {
            if ($model->getOriginal('avg_unit_cost') !== $model->avg_unit_cost || $model->avg_unit_cost) {
                event(new IngredientCostWasUpdated($model->id, $model->cost_added_at));
                // пересчитать стоимость рецептов с этим ингредиентом
            }

            if (!$model->getOriginal('is_declined') && $model->is_declined) {
                // Если ингредиент отклонен, то все рецепты надо перепроверить
                $model->recipes()
                    ->update([
                        'checked_at'   => null,
                        'checked_by'   => null,
                        'check_reason' => Recipe::INGREDIENT_DECLINED,
                    ]);
            }

            \Log::channel('ingredients')->info($model);
        });

        Ingredient::updating(function (Ingredient $model) {
            if ($model->getOriginal('units') !== $model->units) {
                $avgWeight = $model->getOriginal('units') === 'шт' ? $model->getOriginal('avg_piece_weight') : $model->avg_piece_weight;
                if (!$avgWeight) {
                    $avgWeight = 1;
                }
                dispatch_now(
                    new RecalculateIngredientAmountsAfterUnitsTransform(
                        $model->id,
                        $model->getOriginal('units'),
                        $model->units,
                        $avgWeight
                    )
                );
            }
        });

        Ingredient::updated(function (Ingredient $model) {
            if ($model->getOriginal('units') !== $model->units) {
                // todo переименовать задачу под конкретное действие: обновление флага проверки
                dispatch(new ResetRecipeCheckFlag($model->id));
            }
            if (
                $model->getOriginal('vegan_component') !== $model->vegan_component ||
                $model->getOriginal('vegetarian_component') !== $model->vegetarian_component
            ) {
                dispatch(new RecheckRecipesForVegetarianPropertiesByIngredientId($model->id));
            }
        });

        Ingredient::deleting(function (Ingredient $model) {
            $model->load('recipes');
            $recipeIds = $model->recipes->pluck('id')->all();
            dispatch(new RecheckRecipesForVegetarianProperties($recipeIds));
        });

        Ingredient::deleted(function (Ingredient $model) {
            dispatch(new ResetRecipeCheckFlag($model->id));
        });
    }
}
