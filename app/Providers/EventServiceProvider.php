<?php

namespace App\Providers;

use App\Events\IngredientCostWasUpdated;
use App\Events\NewRecipeWasAdded;
use App\Events\RecipeIngredientsWasUpdated;
use App\Listeners\RecipeIngredientsUpdateHandler;
use App\Listeners\ProcessCostUpdateTimestamp;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        IngredientCostWasUpdated::class => [
            ProcessCostUpdateTimestamp::class,
        ],
        NewRecipeWasAdded::class => [
            RecipeIngredientsUpdateHandler::class,
        ],
        RecipeIngredientsWasUpdated::class => [
            RecipeIngredientsUpdateHandler::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
