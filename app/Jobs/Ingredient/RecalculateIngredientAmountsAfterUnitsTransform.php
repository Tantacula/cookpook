<?php

namespace App\Jobs\Ingredient;

use App\Exceptions\IngredientUnitConversionException;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

// при сохранении ингредиента с измененными единицами пересчитывает содержание с учетом этих единиц в рецептах

class RecalculateIngredientAmountsAfterUnitsTransform implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $ingredientId;
    private $initialUnits;
    private $unitsConvertTo;
    private $avgPieceWeightInGramms;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        int $ingredientId,
        $initialUnits = 'шт',
        $unitsConvertTo = 'гр',
        $avgPieceWeightInGramms = 1
    ) {
        $this->ingredientId = $ingredientId;
        $this->initialUnits = $initialUnits;
        $this->unitsConvertTo = $unitsConvertTo;
        $this->avgPieceWeightInGramms = $avgPieceWeightInGramms;
    }

    /**
     * @throws IngredientUnitConversionException
     */
    public function handle()
    {
        if (!self::conversionAvailable($this->initialUnits, $this->unitsConvertTo)) {
            throw new IngredientUnitConversionException("Нельзя конвертировать $this->initialUnits в $this->unitsConvertTo.");
        }

        switch ($this->initialUnits) {
            case 'шт':
                if ($this->unitsConvertTo === 'кг') {
                    \DB::table('ingredient_recipe')
                        ->where('ingredient_id', $this->ingredientId)
                        ->update([
                            // "* 1.0" - is a workaround for sqlite in tests
                            'amount' => \DB::raw( 'amount * 1.0 * ' . $this->avgPieceWeightInGramms . ' / 1000' )
                        ]);
                }
                if ($this->unitsConvertTo === 'гр') {
                    // предположим, штука весит 200 грамм. и в рецепте полштуки
                    // тогда amount начальный равен 0.5 а конечный должен быть 100
                    // avgPieceWeightInGramms * amount
                    \DB::table('ingredient_recipe')
                        ->where('ingredient_id', $this->ingredientId)
                        ->update([
                            'amount' => \DB::raw( 'amount * 1.0 * ' . $this->avgPieceWeightInGramms )
                        ]);
                }
                break;
            case 'гр':
                if ($this->unitsConvertTo === 'кг') {
                    // разделить на тысячу
                    \DB::table('ingredient_recipe')
                        ->where('ingredient_id', $this->ingredientId)
                        ->update([
                            'amount' => \DB::raw( 'amount * 1.0 / 1000' )
                        ]);
                }
                if ($this->unitsConvertTo === 'шт') {
                    // опять же, штука весит 200 гр и в рецепте было добавлено 100 гр ингредиента
                    // тогда amount начальный 100 в таблице, а конечный должен быть 0.5
                    \DB::table('ingredient_recipe')
                        ->where('ingredient_id', $this->ingredientId)
                        ->update([
                            'amount' => \DB::raw( 'amount * 1.0 / ' . $this->avgPieceWeightInGramms )
                        ]);
                }
                break;
            case 'кг':
                if ($this->unitsConvertTo === 'гр') {
                    // умножить на тысячу
                    \DB::table('ingredient_recipe')
                        ->where('ingredient_id', $this->ingredientId)
                        ->update([
                            'amount' => \DB::raw( 'amount * 1.0 * 1000' )
                        ]);
                }
                if ($this->unitsConvertTo === 'шт') {
                    // опять же, штука весит 2кг (2000 гр) и в рецепте было добавлено 0.2шт ингредиента (0.4 кг)
                    // тогда amount начальный 0.4 в таблице, а конечный должен быть 0.2
                    \DB::table('ingredient_recipe')
                        ->where('ingredient_id', $this->ingredientId)
                        ->update([
                            'amount' => \DB::raw( 'amount * 1.0 * 1000 / ' . $this->avgPieceWeightInGramms )
                        ]);
                }
                break;
        }
    }

    public static function conversionAvailable($initialUnits, $unitsConvertTo): bool
    {
        switch ($initialUnits) {
            case 'шт':
                $conversionAvailableTo = ['гр', 'кг'];
                return (in_array($unitsConvertTo, $conversionAvailableTo));
            case 'гр':
                $conversionAvailableTo = ['кг', 'шт'];
                return (in_array($unitsConvertTo, $conversionAvailableTo));
            case 'кг':
                $conversionAvailableTo = ['гр', 'шт'];
                return (in_array($unitsConvertTo, $conversionAvailableTo));
            default:
                return false;
        }
    }
}
