<?php

namespace App\Jobs\Ingredient;

use App\Exceptions\IngredientExistsException;
use App\Exceptions\IngredientValidatorException;
use App\Models\Ingredient;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateIngredient/* implements ShouldQueue*/
{
    //use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    private $data;

    /**
     * @var User
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $ingredientData, User $user)
    {
        $this->data = $ingredientData;
        $this->user = $user;
    }

    /**
     * @param Ingredient $i
     * @return Ingredient
     * @throws IngredientExistsException
     * @throws IngredientValidatorException
     */
    public function handle(Ingredient $i): Ingredient
    {
        $paramsToFill = $i::$requiredParamsForCheckedIngredient;
        $data = collect($this->data);
        $user = $this->user;

        if ($this->ingredientExists($this->data)) {
            throw new IngredientExistsException('Ингредиент с таким названием уже был добавлен.');
        }

        foreach ($paramsToFill as $item) {
            if ($item === 'vegetarian_component' || $item === 'vegan_component' || $item === 'is_loose') {
                $i->{$item} = $data->get($item) ?: false;
            } else {
                $i->{$item} = $data->has($item) ? $data->get($item) : null;
            }
        }
        $i->created_by = $user->id;
        // проверяем, все ли поля, которые пользователь должен был внести добавлены
        $validateUserInput = $i->checkModelFilling($user->role);

        if (!$validateUserInput->isFilled) {
            throw new IngredientValidatorException($validateUserInput->emptyParam);
        }

        $ingredientIsFilledForApprove = $i->isFilledForApprove();

        // если пользователь - модератор или админ и все необходимые поля заполнены - ставим checked
        if ($user->can_approve_ingredients && $ingredientIsFilledForApprove) {
            $i->setCheckedByAttribute($user->id);
        } else {
            // иначе checked = null
            $i->removeCheckedAttribute(Ingredient::INGREDIENT_CREATED);
        }

        $i->save();

        // todo возвращать объект {errors, result}
        return $i;
    }

    /**
     * @param array $data
     * @return bool
     * @throws IngredientValidatorException
     */
    private function ingredientExists(array $data): bool
    {
        if (!array_key_exists('name', $data)) {
            throw new IngredientValidatorException('Имя ингредиента не задано.');
        }
        $ingredient = app()->make(Ingredient::class);
        return !is_null($ingredient->where('name', $data['name'])->first());
    }
}
