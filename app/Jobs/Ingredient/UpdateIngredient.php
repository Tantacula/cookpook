<?php

namespace App\Jobs\Ingredient;

use App\Exceptions\ResourceAccessException;
use App\Models\Ingredient;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateIngredient/* implements ShouldQueue*/
{
//    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    private $id;
    /**
     * @var array
     */
    private $data;

    /**
     * @var User
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $id, array $ingredientData, User $user)
    {
        $this->id = $id;
        $this->data = $ingredientData;
        $this->user = $user;
    }

    /**
     * @param Ingredient $i
     * @return Ingredient
     * @throws ResourceAccessException
     */
    public function handle(Ingredient $i): Ingredient
    {
        $paramsToFill = $i::$requiredParamsForCheckedIngredient;
        $data = collect($this->data);
        $user = $this->user;
        /** @var Ingredient $ingredient */
        $ingredient = $i->findOrFail($this->id);

        // Менять ингредиент после одобрения не может никто, кроме админа. Модератор может лишь поменять стоимость
        if (!$user->can_approve_ingredients) {
            if ($ingredient->is_checked) {
                throw new ResourceAccessException('Ингредиент не может быть отредактирован. Обратитесь к администратору.');
            } elseif ($ingredient->created_by !== $user->id) {
                throw new ResourceAccessException('Нет прав для редактирования ингредиента.');
            }
        }

        // если ингредиент уже был проверен, заполняем лишь стоимость
        if ($ingredient->is_checked && $user->role !== 'admin') {
            $paramsToFill = ['avg_unit_cost'];
        }

        foreach ($paramsToFill as $item) {
            if ($item === 'vegetarian_component' || $item === 'vegan_component' || $item === 'is_loose') {
                $ingredient->{$item} = $data->get($item) ?: false;
            } else {
                $ingredient->{$item} = $data->has($item) ? $data->get($item) : $ingredient->{$item};
            }
        }

        $ingredientIsFilledForApprove = $ingredient->isFilledForApprove();
        if ($user->can_approve_ingredients && $ingredientIsFilledForApprove) {
            $ingredient->setCheckedByAttribute($user->id);
        } else {
            $ingredient->removeCheckedAttribute(Ingredient::INGREDIENT_UPDATED);
        }
        $ingredient->save();

        return $ingredient;
    }
}
