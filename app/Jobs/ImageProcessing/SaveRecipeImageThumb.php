<?php

namespace App\Jobs\ImageProcessing;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SaveRecipeImageThumb /*implements ShouldQueue*/
{
//    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $dir;
    private $filename;

    /**
     * SaveRecipeImageThumb constructor.
     * @param string $dir - папка пользователя (Не абсолютная).
     * @param string $filename - имя файла.
     */
    public function __construct(string $dir, string $filename)
    {
        $this->dir = $dir;
        $this->filename = $filename;
    }

    /**
     * Execute the job.
     */
    public function handle(): string
    {
        $image = \Image::make($this->getSourceFilepath($this->dir, $this->filename));
        $destinationDir = public_path(config('filesystems.disks.recipes-thumb.root') . DIRECTORY_SEPARATOR . $this->dir);

        $maxWidth = config('image.recipes.thumb.width');
        $maxHeight = config('image.recipes.thumb.height');

        // превьюшки уменьшаем по меньшей координате, чтобы не портилось качество
        if ($image->width() < $image->height()) {
            if ($image->getWidth() > $maxWidth) {
                $image->widen($maxWidth);
            }
        } else {
            if ($image->getHeight() > $maxHeight) {
                $image->heighten($maxHeight);
            }
        }

        // похоже, надо сперва создать папку пользователя, а затем сохранять

        if (!is_dir($destinationDir)) {
            mkdir($destinationDir, 0775, true);
        }

        $image->encode(config('image.recipes.type'), 85)
            ->save($destinationDir . DIRECTORY_SEPARATOR . $this->filename);

        // без этого из командной строки нельзя работать с файлами от не www-data пользователя
        chmod($destinationDir . DIRECTORY_SEPARATOR . $this->filename, 0775);

        return $this->dir . DIRECTORY_SEPARATOR . $this->filename;
    }

    private function getSourceFilepath($path, $name)
    {
        return public_path(config('filesystems.disks.recipes.root') . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . $name);
    }
}
