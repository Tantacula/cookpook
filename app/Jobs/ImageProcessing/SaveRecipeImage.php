<?php

namespace App\Jobs\ImageProcessing;

use Illuminate\Bus\Queueable;
use Illuminate\Http\UploadedFile;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SaveRecipeImage /*implements ShouldQueue*/
{
    // use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $image;
    private $filename;
    private $dir;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UploadedFile $image, $filename, $dir)
    {
        $this->image = $image;
        $this->filename = $filename;
        $this->dir = $dir;
    }

    /**
     * Execute the job.
     *
     * @return string
     */
    public function handle(): string
    {
        $maxWidth = config('image.recipes.size.width');
        $maxHeight = config('image.recipes.size.height');
        $filetype = config('image.recipes.type');
        $filename = $this->filename . '.' . $filetype;
        $destinationDir = public_path(config('filesystems.disks.recipes.root') . DIRECTORY_SEPARATOR . $this->dir);

        $image = \Image::make($this->image)->orientate();

        if ($image->width() > $image->height()) {
            if ($image->getWidth() > $maxWidth) {
                $image->widen($maxWidth);
            }
        } else {
            if ($image->getHeight() > $maxHeight) {
                $image->heighten($maxHeight);
            }
        }

        if (!is_dir($destinationDir)) {
            mkdir($destinationDir, 0775, true);
        }

        $image
            ->encode($filetype, 85)
            ->save($destinationDir . DIRECTORY_SEPARATOR . $filename);

        // без этого из командной строки нельзя работать с файлами от не www-data пользователя
        chmod($destinationDir . DIRECTORY_SEPARATOR . $filename, 0775);

        dispatch(new SaveRecipeImageThumb($this->dir, $this->filename . '.' . $filetype));

        return $this->dir . '/' . $this->filename . '.' . $filetype;
    }
}
