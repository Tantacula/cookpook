<?php

namespace App\Jobs\ImageProcessing;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteRecipeImage/* implements ShouldQueue*/
{
    //use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $imgSrc;

    /**
     * DeleteRecipeImage constructor.
     * @param $imgSrc
     */
    public function __construct($imgSrc)
    {
        $this->imgSrc = $imgSrc;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $filepath = public_path(config('filesystems.disks.recipes.root') . DIRECTORY_SEPARATOR . $this->imgSrc);
        if (file_exists($filepath)) {
            unlink($filepath);
        }
        $thumbPath = public_path(config('filesystems.disks.recipes-thumb.root') . DIRECTORY_SEPARATOR . $this->imgSrc);
        if (file_exists($thumbPath)) {
            unlink($thumbPath);
        }
    }
}
