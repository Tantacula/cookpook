<?php

namespace App\Jobs\Recipe;

use App\Models\Ingredient;
use App\Models\Recipe;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RecheckRecipesForVegetarianPropertiesByIngredientId implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $ingredientId;

    /**
     * RecheckRecipesForVegetarianPropertiesByIngredientId constructor.
     * @param int $ingredientId
     */
    public function __construct(int $ingredientId)
    {
        $this->ingredientId = $ingredientId;
    }


    /**
     * Execute the job.
     * @param Ingredient $ingredientModel
     * @return void
     */
    public function handle(Ingredient $ingredientModel)
    {
        $ingredient = $ingredientModel->find($this->ingredientId);
        if ($ingredient) {
            $ingredient->load('recipes');
            $recipeIds = $ingredient->recipes->pluck('id')->all();
            dispatch(new RecheckRecipesForVegetarianProperties($recipeIds));
        }
    }
}
