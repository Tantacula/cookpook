<?php

namespace App\Jobs\Recipe;

use App\Models\Recipe;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RecheckRecipesForVegetarianProperties implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    private $recipeIds;

    /**
     * RecheckRecipesForVegetarianProperties constructor.
     * @param array $recipeIds
     */
    public function __construct(array $recipeIds = [])
    {
        $this->recipeIds = $recipeIds;
    }


    /**
     * Execute the job.
     * @param Recipe $recipeModel
     * @return void
     */
    public function handle(Recipe $recipeModel)
    {
        $recipes = $recipeModel->with('ingredients')
            ->whereIn('id', $this->recipeIds)
            ->get();

        /** @var Recipe $recipe */
        foreach ($recipes as $recipe) {
            $vegStatus = $recipe->getActualVegStatus();
            $updateRequired = false;
            if ($recipe->is_vegan !== $vegStatus->is_vegan) {
                $updateRequired = true;
                $recipe->is_vegan = $vegStatus->is_vegan;
            }
            if ($recipe->is_vegetarian !== $vegStatus->is_vegetarian) {
                $updateRequired = true;
                $recipe->is_vegetarian = $vegStatus->is_vegetarian;
            }
            if ($updateRequired) {
                $recipe->save();
            }
        }
    }
}
