<?php

namespace App\Jobs\Recipe;

use App\Models\Recipe;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CalculateRecipeComposition implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    private $recipeId;

    /**
     * CalculateRecipeComposition constructor.
     * @param int $recipeId
     */
    public function __construct($recipeId)
    {
        $this->recipeId = $recipeId;
    }

    /**
     * Execute the job.
     * @param Recipe $recipeModel
     * @return void
     */
    public function handle(Recipe $recipeModel)
    {
        /** @var Recipe $recipe */
        $recipe = $recipeModel->find($this->recipeId);
        if (!$recipe) {
            return;
        }
        $recipeChanged = false;

        // обновляем инфу о том, что стоимость ингредиентов обновилась, если требуется
        $updateCostRequired = $recipe->checkCostFieldUpdate();
        if ($updateCostRequired->update_required) {
            $recipe->ingredients_cost_updated_at = $updateCostRequired->ts;
            $recipeChanged = true;
        }

        // обновляем калорийность, если требуется
        $calories = $recipe->calculateCalories();
        if ($calories && (float)$recipe->calories !== (float)$calories) {
            $recipe->calories = $calories;
            $recipeChanged = true;
        }

        // Обновляем общий вес или объем блюда.
        $amount = $recipe->calculateTotalAmount();
        if ($amount && (float)$recipe->total_amount !== $amount) {
            $recipe->total_amount = $amount;
            $recipeChanged = true;
        }

        // считаем бжу на 100 грамм
        $composition = $recipe->calculateComposition();
        if ($composition) {
            if ($composition->proteins !== (float)$recipe->proteins) {
                $recipe->proteins = $composition->proteins;
                $recipeChanged = true;
            }
            if ($composition->fats !== (float)$recipe->fats) {
                $recipe->fats = $composition->fats;
                $recipeChanged = true;
            }
            if ($composition->carbohydrates !== (float)$recipe->carbohydrates) {
                $recipe->carbohydrates = $composition->carbohydrates;
                $recipeChanged = true;
            }
        }

        if ($recipeChanged) {
            $recipe->save();
        }
        dispatch(new RecheckRecipesForVegetarianProperties([$recipe->id]));
    }
}
