<?php

namespace App\Jobs\Recipe;

use App\Exceptions\Eloquent\InvalidAttributeException;
use App\Models\Ingredient;
use App\Models\Recipe;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessRecipesWithUpdatedIngredients implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $ingredientId;

    /**
     * ProcessRecipesWithUpdatedIngredients constructor.
     * @param int $ingredientId
     */
    public function __construct(int $ingredientId)
    {
        $this->ingredientId = $ingredientId;
    }

    /**
     * Execute the job.
     * @param Ingredient $ingredientModel
     * @param Recipe $recipeModel
     * @return void
     */
    public function handle(Ingredient $ingredientModel, Recipe $recipeModel)
    {
        /** @var Ingredient $i */
        $i = $ingredientModel->find($this->ingredientId);
        if(!$i) {
            return;
        }
        try {
            /** @var Recipe $recipe */
            foreach($i->recipes as $recipe) {
                // todo: вынести обнуление checked_at checked_by в метод setCheckReason
                // создать метод setCheckStatus в модели
                $recipe->checked_at = null;
                $recipe->checked_by = null;
                $recipe->setCheckReason($recipeModel::INGREDIENT_UPDATED);
                $recipe->save();
            }
        } catch(InvalidAttributeException $e) {
            \Log::error($e->getMessage());
        }

    }
}
