<?php

namespace App\Console\Commands;

use App\Models\Recipe;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class DeleteUnusedRecipeImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удалить неиспользуемые изображения рецептов.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $imagesRoot = public_path(config('filesystems.disks.recipes.root'));
        $userDirs = scandir($imagesRoot);

        $filesList = [];

        foreach ($userDirs as $dir) {
            // Учитываем только директории с численным названием
            // т.к. они соответствуют названиям, создаваемым в системе по id пользователя
            if (is_numeric($dir) && is_dir($imagesRoot . DIRECTORY_SEPARATOR . $dir)) {
                $filesList = array_merge($filesList, $this->getImagesFromDir($dir));
            }
        }

        foreach ($filesList as $key => $item) {
            if (!$this->isImageDeprecated($item)) {
                unset($filesList[$key]);
            }
        }

        $subdir = date('Y-m-d--H-i-s');
        if(count($filesList) === 0) {
            $this->info('Не найдены лишние изображения.');
            // todo: Превью в данном случае могут содержать лишние изображения.
            return;
        }
        if ($this->confirm('Переместить ' . count($filesList) . ' в корзину?')) {
            $this->moveImagesToTmpStorage($filesList, $subdir);
        }
    }

    private function getImagesFromDir($dir)
    {
        $imagesRoot = public_path(config('filesystems.disks.recipes.root'));
        $fileList = scandir($imagesRoot . DIRECTORY_SEPARATOR . $dir);
        foreach ($fileList as $key => $file) {
            if (!Str::endsWith($file, '.jpg')) {
                unset($fileList[$key]);
            } else {
                $fileList[$key] = $dir . '/' . $fileList[$key];
            }
        }
        return $fileList;
    }

    private function isImageDeprecated($filepath)
    {
        $recipe = Recipe::where('image', $filepath)->first();
        return is_null($recipe);
    }

    private function moveImagesToTmpStorage($list, $subDir)
    {
        $tmpDir = storage_path('images-trash' . DIRECTORY_SEPARATOR . $subDir);
        if (!is_dir($tmpDir)) {
            mkdir($tmpDir, 0777, true);
            mkdir($tmpDir . DIRECTORY_SEPARATOR . 'thumbs', 0777, true);
        }

        foreach ($list as $file) {
            $file = str_replace('/', DIRECTORY_SEPARATOR, $file);
            $pathArr = explode(DIRECTORY_SEPARATOR, $file);
            if ($pathArr) {
                $subDir = $pathArr[0];
                if (!is_dir($tmpDir . DIRECTORY_SEPARATOR . $subDir)) {
                    mkdir($tmpDir . DIRECTORY_SEPARATOR . $subDir, 0777, true);
                    mkdir($tmpDir . DIRECTORY_SEPARATOR . 'thumbs' . DIRECTORY_SEPARATOR . $subDir,
                        0777, true);
                }
            }

            $this->info('Перемещение: ' . $file);
            $thumbPath = config('filesystems.disks.recipes-thumb.root') . DIRECTORY_SEPARATOR . $file;
            // перемещаем основное изображение
            rename(public_path(config('filesystems.disks.recipes.root') . DIRECTORY_SEPARATOR . $file), $tmpDir . DIRECTORY_SEPARATOR . $file);
            // перемещаем превьюшку изображения
            if (file_exists(public_path($thumbPath))) {
                rename(public_path($thumbPath),
                    $tmpDir . DIRECTORY_SEPARATOR . 'thumbs' . DIRECTORY_SEPARATOR . $file);
            }
        }
    }
}
