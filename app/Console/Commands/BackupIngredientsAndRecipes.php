<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\DbDumper\Compressors\GzipCompressor;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Exceptions\CannotSetParameter;
use Spatie\DbDumper\Exceptions\CannotStartDump;
use Spatie\DbDumper\Exceptions\DumpFailed;

class BackupIngredientsAndRecipes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup-recipes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Бэкап таблиц рецептов и ингредиентов.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws DumpFailed
     * @throws CannotStartDump
     * @throws CannotSetParameter
     *
     * @return string
     */
    public function handle(): string
    {
        $backupName = 'dump-' . date('Y-m-d-H-i-s') . '.sql.gz';
        MySql::create()
            ->setDbName(config('database.connections.mysql.database'))
            ->setUserName(config('database.connections.mysql.username'))
            ->setPassword(config('database.connections.mysql.password'))
            ->doNotCreateTables()
            ->includeTables(['ingredients', 'recipes', 'ingredient_recipe', 'cooking_type_recipe'])
            ->useCompressor(new GzipCompressor())
            ->dumpToFile(storage_path('recipes-backup' . DIRECTORY_SEPARATOR . $backupName));

        return $backupName;
    }
}
