<?php

namespace App\Console\Commands;

use App\Jobs\ImageProcessing\SaveRecipeImageThumb;
use App\Models\Recipe;
use Illuminate\Console\Command;

class RegenerateRecipeThumbnails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'regenerate-thumbs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Пересоздание превьюшек рецептов.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Recipe::chunkById(50, function ($chunk) {
            foreach ($chunk as $recipe) {
                $arrPath = explode('/', $recipe->image);
                $filename = array_pop($arrPath);
                $filepath = implode('/', $arrPath);

                $thumbname = dispatch_now(new SaveRecipeImageThumb($filepath, $filename));
                $this->info($thumbname);
            }
        });
    }
}
