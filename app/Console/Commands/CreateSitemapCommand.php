<?php

namespace App\Console\Commands;

use App\Models\Recipe;
use Illuminate\Console\Command;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class CreateSitemapCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создать карту сайта.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = public_path('sitemap.xml');
        $sitemap = Sitemap::create();

        $costs = range(50, 1000, 50);

        foreach ($costs as $amount) {
            $sitemap->add(Url::create('/recipes/portion-cost-under/' . $amount)
                // ->setLastModificationDate(Carbon::now()->startOfMonth())
                ->setPriority(0.8));
        }

        Recipe::chunk(100, function ($chunk) use ($sitemap) {
            foreach ($chunk as $recipe) {
                $sitemap->add(Url::create('/recipes/' . $recipe->id)
                    ->setLastModificationDate($recipe->created_at)
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
                    ->setPriority(0.8));
            }
        });

        $sitemap->writeToFile($path);
    }
}
