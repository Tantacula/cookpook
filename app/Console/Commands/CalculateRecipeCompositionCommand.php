<?php

namespace App\Console\Commands;

use App\Jobs\Recipe\CalculateRecipeComposition;
use App\Models\Recipe;
use Illuminate\Console\Command;

class CalculateRecipeCompositionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculate-recipes-composition';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Пересчитывает БЖУ, вес блюда, калорийность для всех рецептов.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        Recipe::with('ingredients')
            ->whereNull('is_declined')
            ->where(function ($query) {
                $query->whereNotNull('checked_by')
                    ->orWhereNotNull('checked_at');
            })
            ->chunk(50, function ($chunk) {
                foreach ($chunk as $recipe) {
                    dispatch_now(new CalculateRecipeComposition($recipe->id));
                    $this->info("Рецепт '{$recipe-> name}' проверен.");
                }
            });
    }
}
