<?php

namespace App\Console\Commands;

use App\Models\Recipe;
use Illuminate\Console\Command;

class UpdateRecipesCostCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-recipes-cost';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить стоимость рецептов.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Recipe::with('ingredients')
            ->whereNull('is_declined')
            ->where(function ($query) {
                $query->whereRaw('cost_calculated_at <= ingredients_cost_updated_at')
                    ->orWhere(function ($query2) {
                        $query2->whereNull('cost_calculated_at')
                            ->whereNotNull('ingredients_cost_updated_at');
                    });
            })
            ->chunkById(100, function ($recipes) {
                $this->info(json_encode($recipes->pluck('id')));
                /** @var Recipe $recipe */
                foreach ($recipes as $recipe) {
                    $totalCost = $recipe->calculateTotalCost();
                    $recipe->total_cost = $totalCost;
                    $recipe->save();
                }
                // todo при создании ингредиента ограничить добавление единиц измерения
                // todo создать список рецептов. просто рекомендации нескльки храндомных блюд дня
                // todo вверху фильтр, плюс страницы, основанные на wordstat что приготовить
            });
    }
}
