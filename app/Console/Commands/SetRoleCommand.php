<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class SetRoleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set-role
                            {email : Email пользователя}
                            {role : Роль пользователя}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Назначить роль пользователю';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::where('email', $this->argument('email'))->first();
        if (!$user) {
            $this->error('Пользователь с таким емейлом не найден.');
            return;
        }
        $roles = ['user', 'moderator', 'admin'];
        if (!in_array($this->argument('role'), $roles)) {
            $this->error('Роль должна быть: ' . implode(' ,', $roles));
            return;
        }
        $user->role = $this->argument('role');
        $user->save();
        $this->info("Пользователю {$user->name} успешно назначена роль {$this->argument('role')}.");
    }
}
