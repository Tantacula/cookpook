<?php

namespace App\Events;

use App\Contracts\Events\RecipeEventContract;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewRecipeWasAdded implements RecipeEventContract
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $recipeId;

    /**
     * NewRecipeWasAdded constructor.
     * @param int $recipeId
     */
    public function __construct(int $recipeId)
    {
        $this->recipeId = $recipeId;
    }

    /**
     * @return int
     */
    public function getRecipeId(): int
    {
        return $this->recipeId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
