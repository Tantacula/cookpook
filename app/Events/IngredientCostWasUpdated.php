<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class IngredientCostWasUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $ingredientId;
    private $costUpdatedAt;

    /**
     * IngredientCostWasUpdated constructor.
     * @param int $ingredientId
     * @param Carbon $costUpdatedAt
     */
    public function __construct(int $ingredientId, Carbon $costUpdatedAt)
    {
        $this->ingredientId = $ingredientId;
        $this->costUpdatedAt = $costUpdatedAt;
    }

    /**
     * @return int
     */
    public function getIngredientId(): int
    {
        return $this->ingredientId;
    }

    /**
     * @return Carbon
     */
    public function getCostUpdatedAt(): Carbon
    {
        return $this->costUpdatedAt;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
