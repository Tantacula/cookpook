<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\CookingType
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CookingType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CookingType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CookingType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CookingType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CookingType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CookingType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CookingType whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CookingType whereUpdatedAt($value)
 */
	class CookingType extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\DislikedRecipe
 *
 * @property int $id
 * @property int $user_id
 * @property int $recipe_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Recipe $recipe
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DislikedRecipe newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DislikedRecipe newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DislikedRecipe query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DislikedRecipe whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DislikedRecipe whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DislikedRecipe whereRecipeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DislikedRecipe whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DislikedRecipe whereUserId($value)
 */
	class DislikedRecipe extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\FavoriteRecipe
 *
 * @property int $id
 * @property int $user_id
 * @property int $recipe_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Recipe $recipe
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteRecipe newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteRecipe newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteRecipe query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteRecipe whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteRecipe whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteRecipe whereRecipeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteRecipe whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteRecipe whereUserId($value)
 */
	class FavoriteRecipe extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Ingredient
 *
 * @property int $id
 * @property string $name
 * @property string $units
 * @property bool $is_loose
 * @property int|null $amount_in_glass_200ml
 * @property mixed|null $min_amount_to_buy
 * @property string|null $comment
 * @property mixed|null $calories
 * @property mixed|null $proteins
 * @property mixed|null $fats
 * @property mixed|null $carbohydrates
 * @property bool|null $vegan_component
 * @property bool|null $vegetarian_component
 * @property mixed|null $avg_unit_cost
 * @property \Illuminate\Support\Carbon|null $cost_added_at
 * @property int|null $avg_piece_weight
 * @property int|null $created_by
 * @property int|null $checked_by
 * @property \Illuminate\Support\Carbon|null $checked_at
 * @property int|null $declined_by
 * @property string|null $decline_reason
 * @property bool|null $is_declined
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $check_reason
 * @property-read mixed $is_checked
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Recipe[] $recipes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereAmountInGlass200ml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereAvgPieceWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereAvgUnitCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereCalories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereCarbohydrates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereCheckReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereCheckedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereCheckedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereCostAddedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereDeclineReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereDeclinedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereFats($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereIsDeclined($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereIsLoose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereMinAmountToBuy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereProteins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereUnits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereVeganComponent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ingredient whereVegetarianComponent($value)
 */
	class Ingredient extends \Eloquent {}
}

namespace App{
/**
 * App\OAuthProvider
 *
 * @property int $id
 * @property int $user_id
 * @property string $provider
 * @property string $provider_user_id
 * @property string|null $access_token
 * @property string|null $refresh_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OAuthProvider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OAuthProvider newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OAuthProvider query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OAuthProvider whereAccessToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OAuthProvider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OAuthProvider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OAuthProvider whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OAuthProvider whereProviderUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OAuthProvider whereRefreshToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OAuthProvider whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OAuthProvider whereUserId($value)
 */
	class OAuthProvider extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PostponedRecipe
 *
 * @property int $id
 * @property int $user_id
 * @property int $recipe_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Recipe $recipe
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostponedRecipe newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostponedRecipe newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostponedRecipe query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostponedRecipe whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostponedRecipe whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostponedRecipe whereRecipeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostponedRecipe whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostponedRecipe whereUserId($value)
 */
	class PostponedRecipe extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Recipe
 *
 * @property int $id
 * @property int|null $author_id
 * @property string $name
 * @property int|null $category_id
 * @property string $slug
 * @property string $article_text
 * @property string $image
 * @property int $is_published
 * @property int $portions_amount
 * @property mixed|null $total_amount
 * @property mixed|null $total_cost
 * @property mixed|null $portion_cost
 * @property string|null $cost_calculated_at
 * @property \Illuminate\Support\Carbon|null $ingredients_cost_updated_at
 * @property int $cooking_time_minutes
 * @property mixed|null $calories
 * @property mixed|null $proteins
 * @property mixed|null $fats
 * @property mixed|null $carbohydrates
 * @property bool $is_vegan
 * @property bool $is_vegetarian
 * @property int $marks_amount
 * @property int|null $checked_by
 * @property \Illuminate\Support\Carbon|null $checked_at
 * @property string|null $decline_reason
 * @property bool|null $is_declined
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $declined_by
 * @property string|null $check_reason
 * @property-read \App\Models\User|null $author
 * @property-read \App\Models\RecipeCategory|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CookingType[] $cookingTypes
 * @property-read mixed $calories_per_hundred
 * @property-read mixed $image_thumb_url
 * @property-read mixed $image_url
 * @property-read mixed $is_checked
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ingredient[] $ingredients
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe checked()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe similarNames($string)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereArticleText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereCalories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereCarbohydrates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereCheckReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereCheckedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereCheckedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereCookingTimeMinutes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereCostCalculatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereDeclineReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereDeclinedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereFats($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereIngredientsCostUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereIsDeclined($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereIsPublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereIsVegan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereIsVegetarian($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereMarksAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe wherePortionCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe wherePortionsAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereProteins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereTotalAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereTotalCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Recipe whereUpdatedAt($value)
 */
	class Recipe extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\RecipeCategory
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Recipe[] $recipes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeCategory whereUpdatedAt($value)
 */
	class RecipeCategory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string $role
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Recipe[] $dislikedRecipes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Recipe[] $favoriteRecipes
 * @property-read mixed $can_approve_ingredients
 * @property-read mixed $can_approve_recipes
 * @property-read string $photo_url
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OAuthProvider[] $oauthProviders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Recipe[] $postponedRecipes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

