import Vue from 'vue';
import FormInputImage from '~/components/global-nossr/FormInputImage';

Vue.component('form-input-image', FormInputImage);
