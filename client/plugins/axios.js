import axios from 'axios'
import swal from 'sweetalert2'

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

export default ({ app, store, redirect }) => {
  axios.defaults.baseURL = process.env.apiUrl;

  if (process.server) {
    return
  }

  // Request interceptor
  axios.interceptors.request.use(request => {
    request.baseURL = process.env.apiUrl;

    const token = store.getters['auth/token'];

    if (token) {
      request.headers.common['Authorization'] = `Bearer ${token}`
    }

    const locale = store.getters['lang/locale'];
    if (locale) {
      request.headers.common['Accept-Language'] = locale
    }

    return request
  });

  // Response interceptor
  axios.interceptors.response.use(response => {
    const messages = response.data?.messages || [];

    if (messages.length > 0) {
      messages.forEach(item => {
        let message;
        if (item.text) {
          message = item.text;
        } else if (typeof item === 'string') {
          message = item;
        }

        let messageData = {
          type: item.status || 'info',
          title: item.title || 'Сообщение',
          text: message,
          reverseButtons: true,
          confirmButtonText: app.i18n.t('ok'),
          cancelButtonText: app.i18n.t('cancel'),
        };

        swal.fire(messageData);
      });
    }

    if (response.data?.message) {
      console.log('message');
      let message = response.data.message;
      let messageData = {
        type: message.status || 'info',
        title: message.title || 'Сообщение',
        text: (typeof message === 'string') ? message : message.text,
        reverseButtons: true,
        confirmButtonText: app.i18n.t('ok'),
        cancelButtonText: app.i18n.t('cancel'),
      };

      swal.fire(messageData);
    }

    return response;
  });

  axios.interceptors.response.use(response => response, error => {
    const { status } = error.response || {};

    if (status >= 500) {
      const message = error.response?.data?.message ||
        error.response?.statustext ||
        error.response?.error ||
        app.i18n.t('error_alert_text');
      swal.fire({
        type: 'error',
        title: app.i18n.t('error_alert_title'),
        text: message,
        reverseButtons: true,
        confirmButtonText: app.i18n.t('ok'),
        cancelButtonText: app.i18n.t('cancel'),
      });
    }

    if (status === 401) {
      switch (store.getters['auth/check']) {
        case true: {
          swal.fire({
            type: 'warning',
            title: app.i18n.t('token_expired_alert_title'),
            text: app.i18n.t('token_expired_alert_text'),
            reverseButtons: true,
            confirmButtonText: app.i18n.t('ok'),
            cancelButtonText: app.i18n.t('cancel'),
          }).then(() => {
            store.commit('auth/LOGOUT');

            redirect({ name: 'login' })
          });
          break;
        }
        case false: {
          swal.fire({
            type: 'warning',
            title: 'Требуется авторизация',
            text: 'Вам нужно войти на сайт, чтобы совершить это действие.',
            reverseButtons: true,
            confirmButtonText: app.i18n.t('ok'),
            cancelButtonText: app.i18n.t('cancel'),
          })
        }
      }
    }

    return Promise.reject(error)
  });

  //

  axios.interceptors.response.use(response => response, error => {
    const { status } = error.response || {};

    if (status === 422) {
      const title = 'Ошибка ' + error.response.status;
      if (error.response.data?.errors) {
        const errors = error.response.data.errors;
        const keys = Object.keys(errors);
        let errorText = '';
        keys.forEach(name => {
          errors[keys].forEach(err => {
            if (errorText.length > 0) {
              errorText += '<br>';
            }
            errorText += err;
          })
        });
        swal.fire({
          title,
          html: errorText,
          type: 'error',
        });
      }
      if (error.response.data?.message) {
        swal.fire({
          title,
          text: error.response.data.message,
          type: 'error',
        });
      } else if (error.response.data?.error) {
        swal.fire({
          title,
          text: error.response.data.error,
          type: 'error',
        });
      } else if (error.response.statusText) {
        swal.fire({
          title,
          text: error.response.statusText,
          type: 'error',
        });
      }
    }

    return Promise.reject(error);
  });
}
