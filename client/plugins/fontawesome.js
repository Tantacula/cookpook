import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import {
  faUser,
  faLock,
  faSignInAlt,
  faSignOutAlt,
  faCog,
  faDrumstickBite,
  faHeart,
  faBars,
  faClock,
  faThumbsDown,
  faInfoCircle,
  faTimes,
  faChevronDown,
  faChevronUp,
  faFolderOpen,
  faUserTag,
  faPencilAlt,
  faClipboardList,
  faUtensils,
} from '@fortawesome/free-solid-svg-icons';

import {
  faClock as faClockAlt,
} from '@fortawesome/free-regular-svg-icons';

import {
  faGithub,
} from '@fortawesome/free-brands-svg-icons';

library.add(
  faUser,
  faLock,
  faSignInAlt,
  faSignOutAlt,
  faCog,
  faGithub,
  faDrumstickBite,
  faHeart,
  faBars,
  faClock,
  faClockAlt,
  faThumbsDown,
  faInfoCircle,
  faTimes,
  faChevronDown,
  faChevronUp,
  faFolderOpen,
  faUserTag,
  faPencilAlt,
  faClipboardList,
  faUtensils,
);

// config.autoAddCss = false;

Vue.component('fa', FontAwesomeIcon);
