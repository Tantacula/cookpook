import Vue from 'vue';
import TextEditor from '~/components/global-nossr/TextEditor';

Vue.component('text-editor', TextEditor);
