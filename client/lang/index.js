import en from './en.json';
import es from './es.json';
import ru from './ru.json';
import zhCN from './zh-CN.json';

export default { en, es, ru, 'zh-CN': zhCN }
