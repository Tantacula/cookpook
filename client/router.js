import Vue from 'vue';
import Router from 'vue-router';
import { scrollBehavior } from '~/utils';

/* import Home from '~/pages/home'; */
// import Welcome from '~/pages/welcome';

import Login from '~/pages/auth/login';
import Register from '~/pages/auth/register';
import PasswordReset from '~/pages/auth/password/reset';
import PasswordRequest from '~/pages/auth/password/email';

import Settings from '~/pages/settings/index';
import SettingsProfile from '~/pages/settings/profile';
import SettingsPassword from '~/pages/settings/password';

import Dashboard from '~/pages/dashboard/index';
import AddRecipe from '~/pages/dashboard/recipes/add';
import MyRecipes from '~/pages/dashboard/recipes/index';

// moderator
import ApproveRecipes from '~/pages/dashboard/moderator/approve-recipes';
import ApproveIngredients from '~/pages/dashboard/moderator/approve-ingredients';

// admin
import AdminRecipesList from '~/pages/dashboard/admin/recipes/index';
import AdminIngredientsList from '~/pages/dashboard/admin/ingredients/index';

// all
import RecipesMainPage from '~/pages/recipes/index';
import RecipesRecommendations from '~/pages/recipes/recommendations';
import RecipesWithPortionCostUnder from '~/pages/recipes/with-portion-cost-under';
import RecipesFavorites from '~/pages/recipes/favorites';
import RecipesPostponed from '~/pages/recipes/postponed';
import Recipe from '~/pages/recipes/recipe';

import Privacy from '~/pages/docs/privacy-policy';

Vue.use(Router);

const routes = [
  /* { path: '/', name: 'welcome', component: Welcome }, */
  /* { path: '/home', name: 'home', component: Home }, */

  {
    path: '/',
    redirect: { name: 'recipes.recommendations' },
  },

  {
    path: '/privacy-policy',
    name: 'privacy',
    component: Privacy,
  },

  { path: '/login', name: 'login', component: Login },
  { path: '/register', name: 'register', component: Register },
  { path: '/password/reset', name: 'password.request', component: PasswordRequest },
  { path: '/password/reset/:token', name: 'password.reset', component: PasswordReset },

  {
    path: '/settings',
    component: Settings,
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: SettingsProfile },
      { path: 'password', name: 'settings.password', component: SettingsPassword },
    ],
  },
  {
    path: '/dashboard',
    component: Dashboard,
    // name: 'dashboard',
    children: [
      {
        path: '',
        redirect: { name: 'dashboard.recipes.my' },
      },
      {
        path: 'admin/recipes/list',
        name: 'dashboard.admin.recipes.list',
        component: AdminRecipesList,
        props: (route) => ({ page: Number(route.query.page || 1) }),
      },
      {
        path: 'admin/ingredients/list',
        name: 'dashboard.admin.ingredients.list',
        component: AdminIngredientsList,
        props: (route) => ({ page: Number(route.query.page || 1) }),
      },
      {
        path: 'recipes/my',
        name: 'dashboard.recipes.my',
        component: MyRecipes,
      },
      {
        path: 'recipes/add',
        name: 'dashboard.recipes.add',
        component: AddRecipe,
      },
      {
        path: 'recipes/approve',
        name: 'dashboard.recipes.approve',
        component: ApproveRecipes,
      },
      {
        path: 'ingredients/approve',
        name: 'dashboard.ingredients.approve',
        component: ApproveIngredients,
      },
    ],
  },
  {
    path: '/recipes',
    component: RecipesMainPage,
    children: [
      {
        path: '', redirect: { name: 'recipes.recommendations' },
      },
      {
        path: 'search',
        name: 'recipes.recommendations',
        component: RecipesRecommendations,
      },
      {
        path: 'portion-cost-under/:cost',
        name: 'recipes.portion-cost-under',
        component: RecipesWithPortionCostUnder,
      },
      {
        path: 'favorites',
        name: 'recipes.favorites',
        component: RecipesFavorites,
      },
      {
        path: 'postponed',
        name: 'recipes.postponed',
        component: RecipesPostponed,
      },
      // этот роут должен лежать самым последним
      {
        path: ':recipeId',
        name: 'recipe',
        component: Recipe,
      },
    ],
  },
];

export function createRouter () {
  return new Router({
    routes,
    scrollBehavior,
    mode: 'history',
  })
}
