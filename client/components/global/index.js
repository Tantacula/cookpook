import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

Vue.use(BootstrapVue);

const requireContext = require.context('./', false, /.*\.(vue)$/);

requireContext.keys().forEach(file => {
  const Component = requireContext(file).default;

  if (Component.name) {
    Vue.component(Component.name, Component)
  }
});
