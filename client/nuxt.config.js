require('dotenv').config();

const polyfills = [
  'Promise',
  'Object.assign',
  'Object.values',
  'Array.prototype.find',
  'Array.prototype.findIndex',
  'Array.prototype.includes',
  'String.prototype.includes',
  'String.prototype.startsWith',
  'String.prototype.endsWith',
];

export default {
  // mode: 'spa',

  srcDir: __dirname,

  env: {
    apiUrl: process.env.APP_URL,
    appName: process.env.APP_NAME || 'Banket',
    appLocale: process.env.APP_LOCALE || 'ru',
    githubAuth: !!process.env.GITHUB_CLIENT_ID,
  },

  head: {
    title: process.env.APP_NAME,
    // если объявлять через стрелочную, серверу не хватает памяти
    titleTemplate: function (title) {
      return title && title !== process.env.appName ? `${title} - ${process.env.appName}` : process.env.appName;
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Roboto:400,400i,600,600i&display=swap&subset=cyrillic',
      },
    ],
    script: [
      { src: `https://cdn.polyfill.io/v2/polyfill.min.js?features=${polyfills.join(',')}` },
    ],
  },

  loading: { color: '#007bff' },

  router: {
    middleware: ['locale', 'check-auth'],
  },

  css: [
    { src: '~assets/sass/app.scss', lang: 'scss' },
  ],

  plugins: [
    { src: '~/plugins/vue-tags-input', ssr: false },
    { src: '~/plugins/vue-text-editor', ssr: false },
    { src: '~/plugins/vue-form-input-image', ssr: false },
    { src: '~/plugins/vue-slider', ssr: false },
    { src: '~/plugins/event-hub' },
    '~components/global',
    '~plugins/i18n',
    '~plugins/vform',
    '~plugins/axios',
    '~plugins/fontawesome', // ,
    // '~plugins/nuxt-client-init',
    // { src: '~plugins/bootstrap', ssr: false },
    { src: '~plugins/ga.js', ssr: false },
  ],

  bootstrapVue: {
    components: [
      'BCard',
      'BCardText',
      'BCardBody',
      'BDropdown',
      'BDropdownItemButton',
      'BDropdownDivider',
      'BDropdownItem',
      'BFormSelect',
      'BFormCheckbox',
      'BInputGroup',
      'BInputGroupAppend',
      'BFormInput',
      'BFormFile',
      'BNavbarToggle',
      'BCollapse',
      'BPopover',
      'BButton',
      'BButtonGroup',
      'BModal',
    ],
    bootstrapCSS: false,
    bootstrapVueCSS: false,
  },

  modules: [
    '@nuxtjs/router',
    '~/modules/spa',
    'bootstrap-vue/nuxt',
  ],

  build: {
    extractCSS: true,
    babel: {
      babelrc: true,
    },
    analyze: false,
    // если серверу не хватает памяти для build команды
    // optimization: {
    //   minimize: false,
    // },
    extend (config, ctx) {
      ctx.loaders.imgUrl.limit = 2048;

      if (ctx.isClient) {
        // config.devtool = '#source-map';
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        }, {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },
};
