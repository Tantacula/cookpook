import swal from 'sweetalert2';

export default ({ store, redirect }) => {
  const role = store.getters?.['auth/user']?.role;
  const requiredRoles = ['admin'];
  if (requiredRoles.indexOf(role) === -1) {
    swal.fire({
      type: 'error',
      text: 'У вас нет доступа к этой странице',
      confirmButtonText: 'Закрыть',
    });
    return redirect({ name: 'recipes.recommendations' });
  }
}
