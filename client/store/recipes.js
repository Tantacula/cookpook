import axios from 'axios';

let preferencesIdsLoaded = false;

// state
export const state = () => ({
  categories: [],
  ingredients: [],
  cookingTypes: [],
  recipes: [],
  favoriteIds: [],
  postponedIds: [],
  recipesRecommendations: [],
  recommendationsAddedAt: 0,
});

// getters
export const getters = {
  categories: state => state.categories,
  ingredients: state => state.ingredients,
  cookingTypes: state => state.cookingTypes,
  recipes: state => state.recipes,
  recipesRecommendations: state => state.recipesRecommendations,
  recommendationsAddedAt: state => state.recommendationsAddedAt,
  favoriteIds: state => state.favoriteIds,
  postponedIds: state => state.postponedIds,
};

// mutations
export const mutations = {
  SET_CATEGORIES (state, { categories }) {
    state.categories = categories;
  },
  SET_INGREDIENTS (state, { ingredients }) {
    state.ingredients = ingredients;
  },
  ADD_INGREDIENT (state, { ingredient }) {
    state.ingredients.push(ingredient);
  },
  UPDATE_INGREDIENT (state, { ingredient }) {
    const idx = state.ingredients.findIndex(item => item.id === ingredient.id);
    if (idx !== -1) {
      state.ingredients.splice(idx, 1, ingredient);
    }
  },
  SET_COOKING_TYPES (state, { cookingTypes }) {
    state.cookingTypes = cookingTypes;
  },
  SET_RECIPES (state, { recipes }) {
    state.recipes = recipes;
  },
  SET_RECIPES_RECOMMENDATIONS (state, { recipes }) {
    state.recipesRecommendations = recipes;
  },
  SET_RECOMMENDATIONS_ADDED_AT (state, { ts }) {
    state.recommendationsAddedAt = ts;
  },
  SET_FAVORITE_IDS (state, { ids }) {
    state.favoriteIds = ids;
  },
  ADD_FAVORITE_ID (state, { id }) {
    if (state.favoriteIds.indexOf(id) === -1) {
      state.favoriteIds.push(id);
    }
  },
  REMOVE_FAVORITE_ID (state, { id }) {
    const idx = state.favoriteIds.indexOf(id);
    if (idx !== -1) {
      state.favoriteIds.splice(idx, 1);
    }
  },
  SET_POSTPONED_IDS (state, { ids }) {
    state.postponedIds = ids;
  },
  ADD_POSTPONED_ID (state, { id }) {
    if (state.postponedIds.indexOf(id) === -1) {
      state.postponedIds.push(id);
    }
  },
  REMOVE_POSTPONED_ID (state, { id }) {
    const idx = state.postponedIds.indexOf(id);
    if (idx !== -1) {
      state.postponedIds.splice(idx, 1);
    }
  },
  ADD_RECIPE (state, { recipe }) {
    state.recipes.push(recipe);
  },
};

// actions
export const actions = {
  /*  setCategories ({ commit }, { categories }) {
      commit('SET_CATEGORIES', { categories });
    }, */
  async getCategoriesFromServer ({ commit }) {
    try {
      const { data } = await axios.get('/api/recipe-categories');
      commit('SET_CATEGORIES', { categories: data.categories });
    } catch (e) {
    }
  },
  async getIngredientsFromServer ({ commit }) {
    try {
      const { data } = await axios.get('/api/ingredient');
      commit('SET_INGREDIENTS', { ingredients: data.ingredients });
    } catch (e) {
    }
  },
  addIngredient ({ commit }, ingredient) {
    commit('ADD_INGREDIENT', { ingredient });
  },
  updateIngredient ({ commit }, ingredient) {
    commit('UPDATE_INGREDIENT', { ingredient });
  },
  async getCookingTypesFromServer ({ commit }) {
    try {
      const { data } = await axios.get('/api/cooking-types');
      commit('SET_COOKING_TYPES', { cookingTypes: data.cookingTypes });
    } catch (e) {
    }
  },
  setRecipes ({ commit }, { recipes }) {
    commit('SET_RECIPES', { recipes });
  },
  setRecipesRecommendations ({ commit }, { recipes }) {
    commit('SET_RECOMMENDATIONS_ADDED_AT', { ts: Date.now() });
    commit('SET_RECIPES_RECOMMENDATIONS', { recipes });
  },
  async getRecipeFromServer ({ commit }, { recipeId }) {
    try {
      const { data: { data: recipe } } = await axios.get('/api/recipe/' + recipeId);
      commit('ADD_RECIPE', { recipe });
    } catch (e) {
    }
  },

  async getUserPreferencesFromServer ({ commit }, { initialLoad = false }) {
    if (initialLoad && preferencesIdsLoaded) {
      return;
    }
    preferencesIdsLoaded = true;
    const { data } = await axios.get('/api/recipe/user-preferences');
    commit('SET_FAVORITE_IDS', { ids: data.favoriteIds });
    commit('SET_POSTPONED_IDS', { ids: data.postponedIds });
  },
  async sendAddToFavoriteRequest ({ commit }, { id }) {
    await axios.post('/api/recipe/favorites/add', {
      recipe_id: id,
    });
    commit('ADD_FAVORITE_ID', { id });
  },
  async sendRemoveFromFavoriteRequest ({ commit }, { id }) {
    await axios.post('/api/recipe/favorites/remove', {
      recipe_id: id,
    });
    commit('REMOVE_FAVORITE_ID', { id });
  },
  async sendAddToPostponedRequest ({ commit }, { id }) {
    await axios.post('/api/recipe/postponed/add', {
      recipe_id: id,
    });
    commit('ADD_POSTPONED_ID', { id });
  },
  async sendRemoveFromPostponedRequest ({ commit }, { id }) {
    await axios.post('/api/recipe/postponed/remove', {
      recipe_id: id,
    });
    commit('REMOVE_POSTPONED_ID', { id });
  },
};
