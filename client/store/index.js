import Cookies from 'js-cookie'
import { cookieFromRequest } from '~/utils'

export const actions = {
  nuxtServerInit ({ commit }, { req }) {
    const token = cookieFromRequest(req, 'token');
    if (token) {
      commit('auth/SET_TOKEN', token)
    }

    commit('lang/SET_LOCALE', { locale: 'ru' });
    // const locale = cookieFromRequest(req, 'locale');
    // if (locale) {
    //   commit('lang/SET_LOCALE', { locale })
    // }
  },

  nuxtClientInit ({ commit }) {
    const token = Cookies.get('token');
    if (token) {
      commit('auth/SET_TOKEN', token)
    }

    commit('lang/SET_LOCALE', { locale: 'ru' });
    // const locale = Cookies.get('locale');
    // if (locale) {
    //   commit('lang/SET_LOCALE', { locale })
    // }
  },
};
